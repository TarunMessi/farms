import 'react-native-gesture-handler';
import React from 'react'
import { View, Text } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from './src/Home';
import Announcements from './src/Announcements';
import ContactInfo from './src/ContactInfo';
import SubmitRequest from './src/SubmitRequest';
import Settings from './src/Settings';
import More from './src/More';
import Calender from './src/Calender';
import Documents from './src/Documents';
import Notifications from './src/Notifications';
import PoolPass from './src/PoolPass';
import DetailAnnouncements from './src/component/announcements/DetailAnnouncements';
import colors from './src/styles/theme';
import PDFViewer from './src/component/PDFViewer';
import WebPage from './src/component/WebPage';
import DetailEvent from './src/component/calendar/DetailEvent';
 const Stack = createStackNavigator();
function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName= 'home' screenOptions={{
        headerStyle: {
          backgroundColor:'#436db1',
          shadowColor: 'transparent',
          elevation: 0
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          alignSelf: 'center',
          textAlign: 'center',
          
        }
      }}>
      <Stack.Screen name="home" component={Home} options={{headerShown:false}}/>
      <Stack.Screen name="announcements" component={Announcements} options={{headerTitle:'News'}} />
      <Stack.Screen name="detailAnnouncements" component={DetailAnnouncements} options={{headerTitle:'DetailAnnouncements'}}/>
      <Stack.Screen name="contactInfo" component={ContactInfo} options={{headerTitle:'Contacts'}}/>
      <Stack.Screen name="submitRequest" component={SubmitRequest} options={{headerTitle:'SubmitRequest'}}/>
      <Stack.Screen name="settings" component={Settings} options={{headerTitle:'Settings'}}/>
      <Stack.Screen name="more" component={More} options={{headerTitle:'More'}}/>
      <Stack.Screen name="calender" component={Calender} options={{headerTitle:'Calender'}}/>
      <Stack.Screen name="poolPass" component={PoolPass} options={{headerTitle:'PoolPass'}}/>
      <Stack.Screen name="documents" component={Documents} options={{headerTitle:'Documents'}}/>
      <Stack.Screen name="notifications" component={Notifications} options={{headerTitle:'Notifications'}}/>
      <Stack.Screen name="PDFViewer" component={PDFViewer} options={{headerTitle:'PDFViewer'}}/>
      <Stack.Screen name="webPage" component={WebPage} options={{headerTitle:'WebPage'}}/>
      <Stack.Screen name="detailEvent" component={DetailEvent} options={{headerTitle:'DetailEvent'}}/>  
      </Stack.Navigator>
    </NavigationContainer>
  );
}
  


export default App
