import React, { Component } from 'react'
import { Text, View, FlatList, StyleSheet, Dimensions, StatusBar, TouchableWithoutFeedback, Linking, TouchableOpacity } from 'react-native'
import Icon from "react-native-vector-icons/FontAwesome5";
import { colors } from './styles/theme';
import stylesTheme from './styles/StyleTheme';
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import AntDesign from "react-native-vector-icons/AntDesign";
import Fontisto from "react-native-vector-icons/Fontisto";
import Foundation from "react-native-vector-icons/Foundation";
import Entypo from "react-native-vector-icons/Entypo";
import Ionicons from "react-native-vector-icons/Ionicons";

const data = [
    {
        key: 'Home Page',
        icon: 'home',
        screen: 'home'
    },
    {
        key: 'News',
        icon: 'news',
        screen: 'announcements'
      },
      {
        key: 'Alerts',
        icon: 'flag',
        screen: 'notifications'
      },
      {
        key: 'Calendar',
        icon: 'calendar-alt',
        screen: 'calender'
      },
      {
        key: 'Documents',
        icon: 'folder-open',
        screen: 'documents'
      },
      {
        key: 'Pay Dues',
        icon: 'dollar',
        screen: 'payDues'
      },
      {
        key: 'Settings',
        icon: 'cog',
        screen: 'settings'
      },
      {
        key: 'Submit A Request',
        icon: 'map-marker-alt',
        screen: 'submitRequest'
      },
      {
        key: 'Useful Numbers',
        icon: 'phone',
        screen: 'contactInfo'
      },
      {
        key: 'Pool/Rec Pass',
        icon: 'id-card',
        screen: 'poolPass'
      },
    ];

class More extends Component {
    constructor(props) {
        super(props);
        const isPortrait = () => {
            const dim = Dimensions.get('screen');
            return dim.height >= dim.width;
        };

        this.state = {
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').height,
            orientation: isPortrait() ? 'portrait' : 'landscape'
        }
        this.onLayout = this.onLayout.bind(this);
        Dimensions.addEventListener('change', () => {
            this.setState({
                orientation: isPortrait() ? 'portrait' : 'landscape'
            });
        });
    }
    componentDidMount() {
        if (Platform.OS === 'android') {
          StatusBar.setBackgroundColor(colors.primary);
          StatusBar.setBarStyle('light-content', true);
        }
    }
    onLayout(e) {
        this.setState({
            width: Dimensions.get('window').width,
            height: this.state.orientation == 'portrait' ? Dimensions.get('window').height : Dimensions.get('window').width,
        });
    }
    renderItem = ({ item, index }) => {
        let itemStyle = {
            backgroundColor: colors.primary,
            alignItems: 'center',
            justifyContent: 'center',
            flex: 1,
            borderWidth: 0,
            borderColor: colors.secondary,
            margin: 0,
            height: this.state.height / 6
        };
        if (item.empty === true) {
            return <View style={itemStyle} />;
        }
        const screen = item.screen
        if (screen == 'payDues') {
            return (
                <TouchableWithoutFeedback onPress={() => {
                    Linking.openURL(`https://www.bbt.com/small-business/payments-and-processing/association-services/homeowner-payments-through-association-services.html`);
                }}>
                    <View style={[styles.item, { height: this.state.height / 6 }]}>
                        <Foundation style={styles.dollarIcon} name={item.icon} />
                        <Text style={styles.itemText}>{item.key}</Text>
                    </View>
                </TouchableWithoutFeedback>
            );
        }
         else if(screen=='announcements') {
            return (
              <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate(screen)}>
                <View style={[styles.item, { height: this.state.height / 6 }]}>
                  <Entypo style={styles.itemIcon} name={item.icon} />
                  <Text style={styles.itemText}>{item.key}</Text>
                </View>
              </TouchableWithoutFeedback>
            );
      
                      } 
              else 
            { 
            return (
                <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate(screen)}>
                    <View style={[styles.item, { height: this.state.height / 6 }]}>
                        <Icon style={styles.itemIcon} name={item.icon} />
                        <Text style={styles.itemText}>{item.key}</Text>
                    </View>
                </TouchableWithoutFeedback>
            );
           }
    };
    render() {
        return (
            <View onLayout={this.onLayout} style={styles.container}>
                <StatusBar barStyle="light-content" backgroundColor={colors.primary} />
                <FlatList
                    data={data}
                    renderItem={this.renderItem}
                    numColumns={3}
                    extraData={this.state}
                ></FlatList>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.primary,
        width: '100%',
        flex: 1
    },
    cell: {
        alignItems: 'center',
        justifyContent: 'center',
        width: Dimensions.get('window').width / 4,
        height: Dimensions.get('window').height / 4
    },
    iconStyle: {
        color: '#fff',
        fontSize: 50,
        padding: 10
    },
    itemText: {
        color: '#fff',
        textAlign: 'center',
        fontSize: 12,
        paddingTop: 5,
        fontFamily: colors.fontFamily
    },
    itemIcon: {
        color: colors.separatorLine,
        fontSize: 30,
    },
    item: {
        backgroundColor: colors.primary,
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        borderWidth: 0,
        borderColor: colors.secondary,
        margin: 0,
        height: Dimensions.get('window').height / 6, // approximate a square
    },
    dollarIcon:{
        color: colors.separatorLine,
        fontSize: 40
      },
})
export default More;
