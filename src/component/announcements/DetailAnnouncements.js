import 'react-native-get-random-values';
import React, { Component } from 'react'
import { View, StatusBar, TouchableOpacity, Platform, Linking } from 'react-native';
import { WebView } from 'react-native-webview';
import { colors } from '../../styles/theme'
import Icon from "react-native-vector-icons/FontAwesome5";
import stylesTheme from '../../styles/StyleTheme';
const WORDPRESS_URL ='http://localhost/TavistockFarms'
const headerTitleStyles = {
    color : '#fff',
    alignSelf: 'center',
    textAlign: 'center'
}
export default class DetailAnnouncements extends Component {

    constructor(props){
        super (props)
    }
    
    render() {
        const { data } = this.props.route.params;
        let html = '';
        if (Platform.OS === 'android'){
            html = `<style>.normal-content section.moko,.normal-content>:not(section){padding:5px 20px}.normal-content header.moko{padding:5px 20px;background:#eee;border-top:solid 1px #ddd;border-bottom:solid 1px #ddd}.normal-content header.moko h2{margin:0 0 0 -10px;font-style:italic;font-size:21px}.normal-content section.moko h1{font-size:18px}.normal-content section.moko h2{font-size:16px}.normal-content section.moko h3{font-size:14px}.normal-content section.moko h1,.normal-content section.moko h2,.normal-content section.moko h3{margin:.5em 0}.normal-content section.moko p{font-size:14px;line-height:1.3}.normal-content section.moko blockquote{margin:0}.normal-content section.moko img{max-width:100%;margin:5px;border-radius:5px}.normal-content section.moko th{font-size:14px;word-break:break-word}.normal-content section.moko td{font-size:12px;padding-bottom:15px}</style><body><div id="announcements" class="normal-content">`;
        }else{
            html = `<style>.normal-content section.moko,.normal-content>:not(section){padding:5px 20px}.normal-content header.moko{padding:5px 20px;background:#eee;border-top:solid 1px #ddd;border-bottom:solid 1px #ddd}.normal-content header.moko h2{margin:0 0 0 -10px;font-style:italic;font-size:21px}.normal-content section.moko h1{font-size:18px}.normal-content section.moko h2{font-size:50px}.normal-content section.moko h3{font-size:14px}.normal-content section.moko h1,.normal-content section.moko h2,.normal-content section.moko h3{margin:.5em 0}.normal-content section.moko p{font-size:40px;line-height:1.3}.normal-content section.moko blockquote{margin:0}.normal-content section.moko img{max-width:100%;margin:5px;border-radius:5px}.normal-content section.moko th{font-size:14px;word-break:break-word}.normal-content section.moko td{font-size:12px;padding-bottom:15px}figcaption{font-size:35px;}</style><body><div id="announcements" class="normal-content">`;
        }
            html += `<section class='moko'>`;
            html += `<h2>${data.title}</h2>`
            html += `${data.description}`
            html += `</section>`;
            html += `</div></body>`

        return (
            <View style={{flex:1}}>
                <StatusBar barStyle="light-content" backgroundColor={colors.primary}/>
                {
                    (Platform.OS === 'ios')&&(
                        <WebView
                            originWhitelist={['*']}
                            source={{html: html, baseUrl: WORDPRESS_URL}}
                            style={{ flex: 1 }}
                            ref={ref => {
                                this.webview = ref;
                            }}
                            onShouldStartLoadWithRequest={ async (event) => {
                                if(event.navigationType === 'click'){
                                    if (event.url.slice(0, 4) === 'http' ||
                                        event.url.slice(0, 3) === 'tel' || event.url.slice(0, 6) === 'mailto' ||
                                        event.url.slice(0, 3) === 'geo' || event.url.slice(0, 3) === 'sms'
                                    ) {
                                        this.webview.stopLoading();
                                        Linking.openURL(event.url)
                                        return false;
                                    }
                                    return true
                                }
                            }}
                            renderError={ (e) => {
                                this.webview.setState({viewState: 'IDLE'});
                            }}
                        />
                    )
                }{
                    (Platform.OS === 'android')&&(
                        <WebView
                            originWhitelist={['*']}
                            source={{html: html, baseUrl: WORDPRESS_URL}}
                            style={{ flex: 1 }}
                            ref={ref => {
                                this.webview = ref;
                            }}
                            useWebKit={false} 
                            scalesPageToFit={false}
                            onShouldStartLoadWithRequest={ (event) => {
                                if (event.url.slice(0, 4) === 'http' ||
                                    event.url.slice(0, 3) === 'tel' || event.url.slice(0, 6) === 'mailto' ||
                                    event.url.slice(0, 3) === 'geo' || event.url.slice(0, 3) === 'sms'
                                ) {
                                    Linking.openURL(event.url);
                                    return false;
                                }
                                return true;
                            }}
                        />
                    )
                    
                }
            </View>
        )
    }
}

