import 'react-native-get-random-values';
import React, { Component } from 'react'
import { View, StatusBar, TouchableOpacity, Platform, Linking } from 'react-native';
import { WebView } from 'react-native-webview';
import { colors } from '../../styles/theme'
import Icon from "react-native-vector-icons/FontAwesome5";
import stylesTheme from '../../styles/StyleTheme';
const WORDPRESS_URL='http://localhost/TavistockFarms'
const Entities = require('html-entities').XmlEntities;
const entities = new Entities();

const headerTitleStyles = {
    color: '#fff',
    alignSelf: 'center',
    textAlign: 'center',
    paddingLeft: 20,fontfamily:colors.fontFamily
}
export default class DetailEvent extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        const data = this.props.route.params.data;
        this.props.navigation.setOptions({
            title: `${entities.decode(data.title)}`,
            headerLeft: () =>
                <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                    <View style={{ paddingHorizontal: 10 }}>
                        <Icon name="arrow-left" style={stylesTheme.headerLeftIcon} />
                    </View>
                </TouchableOpacity>,
            headerTitleAlign: 'center',
            headerTitleStyle: headerTitleStyles
        })
        html = `<style>
        html *
            {font-family: "LibreBaskerville-Regular" !important;}
        .normal-content section.moko,.normal-content>:not(section){
            padding:5px 20px
        }
        .normal-content header.moko{
            padding:5px 20px;
            background:#eee;
            border-top:solid 1px #ddd;
            border-bottom:solid 1px #ddd
        }
        .normal-content header.moko h2{
            margin:0 0 0 -10px;
            font-style:italic;
            font-size:${(Platform.OS === 'ios') ? '55px' : '21px'};
        }
        .normal-content section.moko h1{
            font-size:${(Platform.OS === 'ios') ? '50px' : '18px'};
        }
        .normal-content section.moko h2{
            font-size:${(Platform.OS === 'ios') ? '45px' : '16px'};
        }
        .normal-content section.moko h3{
            font-size:${(Platform.OS === 'ios') ? '40px' : '14px'};
        }
        .normal-content section.moko h1,.normal-content section.moko h2,.normal-content section.moko h3{
            margin:.5em 0
        }
        .normal-content section.moko p{
            font-size:${(Platform.OS === 'ios') ? '40px' : '14px'};
            line-height:1.3
        }
        .normal-content section.moko blockquote {
            margin:0
        }
        .normal-content section.moko img{
            max-width:100%;
            margin:5px;
            border-radius:5px
        }
        .normal-content section.moko th{
            font-size:${(Platform.OS === 'ios') ? '40px' : '14px'};
            word-break:break-word
        }
        .normal-content section.moko td{
            font-size:${(Platform.OS === 'ios') ? '35px' : '12px'};
            padding-bottom:15px
        }
        </style><body>`;
        html += `<div id="events" class="normal-content"><section class='moko'>`;
        html += (data.image) ? `<img src="${data.image.sizes[600].url}" style="width:100%" />` : ``;
        html += `<h2>${data.title}</h2>`
        if (Platform.OS === 'ios') {
            html += (data.organizer) ? `<span style="font-style:italic;color:'grey';font-size:35px;">${data.organizer}</span>` : ``;
        } else {
            html += (data.organizer) ? `<span style="font-style:italic;color:'grey';">${data.organizer}</span>` : ``;
        }
        html += data.description;
        if (Platform.OS === 'ios') {
            html += (data.venue && data.venue.venue) ? `<div style = "font-size:35px;"><h2>Location: </h2>` : '';
        } else {
            html += (data.venue && data.venue.venue) ? `<div><h2>Location: </h2>` : '';
        }
        html += (data.venue && data.venue.venue) ? data.venue.venue + '<br>' : '';
        html += (data.venue && data.venue.phone) ? `<a href='tel:${data.venue.phone}'>${data.venue.phone}</a><br>` : '';
        html += data.venue.address || '';
        html += (data.venue) ? '</div>' : '';
        html += `</section>`;
        html += `</div></body>`

        return (
            <View style={{ flex: 1 }}>
                <StatusBar barStyle="light-content" backgroundColor={colors.primary} />
                {
                    (Platform.OS === 'ios') && (
                        <WebView
                            originWhitelist={['*']}
                            source={{ html: html, baseUrl: WORDPRESS_URL }}
                            style={{ flex: 1 }}
                            ref={ref => {
                                this.webview = ref;
                            }}
                            onShouldStartLoadWithRequest={async (event) => {
                                if (event.navigationType === 'click') {
                                    if (event.url.slice(0, 4) === 'http' ||
                                        event.url.slice(0, 3) === 'tel' || event.url.slice(0, 6) === 'mailto' ||
                                        event.url.slice(0, 3) === 'geo' || event.url.slice(0, 3) === 'sms'
                                    ) {
                                        this.webview.stopLoading();
                                        Linking.openURL(event.url)
                                        return false;
                                    }
                                    return true
                                }
                            }}
                            renderError={(e) => {
                                this.webview.setState({ viewState: 'IDLE' });
                            }}
                        />
                    )
                }{
                    (Platform.OS === 'android') && (
                        <WebView
                            originWhitelist={['*']}
                            source={{ html: html, baseUrl: WORDPRESS_URL }}
                            style={{ flex: 1 }}
                            ref={ref => {
                                this.webview = ref;
                            }}
                            useWebKit={false}
                            scalesPageToFit={false}
                            onShouldStartLoadWithRequest={(event) => {
                                if (event.url.slice(0, 4) === 'http' ||
                                    event.url.slice(0, 3) === 'tel' || event.url.slice(0, 6) === 'mailto' ||
                                    event.url.slice(0, 3) === 'geo' || event.url.slice(0, 3) === 'sms'
                                ) {
                                    Linking.openURL(event.url);
                                    return false;
                                }
                                return true;
                            }}
                        />
                    )

                }
            </View>
        )
    }
}
