import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5';
import { colors } from '../../styles/theme'

export default class StepperButton extends Component {

    constructor(props){
        super(props)
        this.state = {
            next:false,
            prev:false,
        }
    }

    submitBtn () {
        return(
            <View style={styles.nextButtonContainer}>
                <TouchableOpacity onPress={() => this.props.onSubmit()}>
                    <View style={styles.button}>
                        <Icon name={'check'}/> 
                        <Text style={styles.buttonText}>Submit</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    nextBtn () {
        return(
            <View style={styles.nextButtonContainer}>
                <TouchableOpacity onPress={() => this.props.onNext() }>
                    <View style={styles.button}>
                        <Icon style={styles.buttonColor} name={'long-arrow-alt-right'}/> 
                        <Text style={[styles.buttonText,styles.buttonColor]}>Next</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    prevBtn () {
        return (
            <View style={styles.prevButtonContainer}>
                <TouchableOpacity onPress={() => this.props.onPrev() }>
                    <View style={styles.button}>
                        <Icon style={styles.buttonColor} name={'long-arrow-alt-left'}/> 
                        <Text style={[styles.buttonText,styles.buttonColor]}>Prev</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        return (
            <View style={styles.buttonContainer}>
             
            {
                this.props.currentPage > 0 && ( this.prevBtn() )
            }
            {
                this.props.currentPage + 1 < this.props.totalPage && ( this.nextBtn())
            }
            {
                this.props.currentPage >= this.props.totalPage - 1 && (this.submitBtn())
            }
            
            </View>
        )
    }
}

const styles = StyleSheet.create({
    nextButtonContainer :{
        alignItems: 'flex-end',
        position: 'absolute',
        right:10
    },
    prevButtonContainer :{
        alignItems: 'flex-start',
        position: 'absolute',
        left:10
    },
    
    buttonContainer:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical:10,
        paddingHorizontal:10
    },
      
    button :{
        alignItems : 'center',
        flexDirection: 'row',
        borderRadius:50,
        borderColor:colors.secondary,
        borderWidth:2,
        padding:8
    },
    buttonColor:{
        color : colors.secondary
    },
    buttonText : {
        paddingLeft: 10,
        fontFamily: colors.fontFamily
    }
})
