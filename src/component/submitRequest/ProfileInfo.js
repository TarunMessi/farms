import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity, KeyboardAvoidingView, ScrollView, Platform } from 'react-native'
// import { Header } from 'react-navigation';
import { Form, Item, Input, Label} from 'native-base';

import { colors } from '../../styles/theme'
import Icon from "react-native-vector-icons/FontAwesome5";


export default class ProfileInfo extends Component {
    constructor(props){
        super(props)

        this.state = {
            name : { value: this.props.mainState.name, error: ''},
            email : { value: this.props.mainState.email, error: ''},
            phone : { value: this.props.mainState.phone, error: ''},
            address: { value: this.props.mainState.phone, error: ''},
            inquiry: { value: this.props.mainState.inquiry, error: ''},
            status: false,
        }
    }
    ShowHideTextComponentView = () =>{

        if(this.state.status == true)
        {
          this.setState({status: false} )
        }else  {
            this.setState({status: true} )
        }
       
      }
    onNext = async () => {

        if( await this.isValid()){
            const props = this.props.mainState
            const currentPosition = props.currentPosition + 1
            this.props.mainState.name = this.state.name.value
            this.props.mainState.email = this.state.email.value
            this.props.mainState.phone = this.state.phone.value
            this.props.mainState.address = this.state.address.value
            this.props.saveFormData(this.props.mainState)
            this.props.onPageChange(currentPosition)
        }
        
    }

    isValid = () => {
        const {name, email, phone, address} = this.state
        return new Promise( resolve => {
            this.validateName(name.value)
            this.validateEmail(email.value)
            this.validatePhone(phone.value)
            this.validateAddress(address.value)

            this.setState({ 
                ...this.state
            },()=>{
                if(name.error == '' && email.error == '' && phone.error == '' && address.error == ''){
                    resolve(true)
                }else{
                    resolve(false)
                }
            });
        }).catch((err) => {
            console.log(err)
        })
    }

    validateName = (value) => {
        if(value ==''){
            this.state['name'].error = 'Name required'
            return false
        }else{
            this.state['name'].error = ''
            return true
        }
    }
    validateEmail = (value) => {
        if(value == ''){
            this.state['email'].error = 'Email required'
            return false
        }else if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value)){
            this.state['email'].error = 'Email is not valid'
            return false
        }else{
            this.state['email'].error = ''
            return true
        }
    }
    validatePhone = (value) => {
        if(value == ''){
            this.state['phone'].error = 'Phone Number required'
            return false
        }else if(!/((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}/.test(value)){
            this.state['phone'].error = 'Phone Number is not valid'
            return false
        }else{
            this.state['phone'].error = ''
            return true
        }
        
    }

    validateAddress = (value) => {
        if(value ==''){
            this.state['address'].error = 'Address required'
            return false
        }else{
            this.state['address'].error = ''
            return true
        }
    }

    onChange(name, value) { 
        if(name == 'phone'){
            let x = value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/)
            value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
            this.validatePhone(value)
        }else if(name == 'name'){
            this.validateName(value)
        }else if(name == 'email'){
            this.validateEmail(value)
        }else if(name == 'address'){
            this.validateAddress(value)
        }else{
            this.state[name].error = '';
        }
        this.state[name].value = value;
      
        this.setState({ 
            ...this.state
        });
      }

    render() {
        const {currentPosition, totalPage } = this.props.mainState
        const {name, email, phone, address} = this.state
       
        return (
            <KeyboardAvoidingView 
            behavior= {(Platform.OS === 'ios')? "padding" : null}
            style={{ flex: 1}}
            keyboardVerticalOffset = {30}>
            <ScrollView style={{flex: 1}}>
                <View style={styles.container}>
                <Form style={styles.formContainer}>
                    <Item floatingLabel error={(name.error)?true:false}>
                        <Label style={{fontFamily: colors.fontFamily}}>Name *</Label>
                        <Input 
                        style={{fontFamily: colors.fontFamily}}
                            value={name.value}
                            onChangeText={(text)=>this.onChange('name',text)}
                            returnKeyType = { "next" }
                            onSubmitEditing={() => { this.emailInput._root.focus() }}
                            blurOnSubmit={false}
                        />
                    </Item>
                    { name.error!=='' && (<Text style={styles.error}>{name.error}</Text>)}
                    
                    <Item floatingLabel error={(email.error)?true:false}>
                        <Label style={{fontFamily: colors.fontFamily}}>Email *</Label>
                        <Input 
                            getRef={input => {
                                this.emailInput = input;
                            }}
                            style={{fontFamily: colors.fontFamily}}
                            value={email.value}
                            keyboardType='email-address'
                            onChangeText={(text)=>this.onChange('email',text)}
                            returnKeyType = { "next" }
                            onSubmitEditing={() => { this.phoneInput._root.focus(); }}
                            blurOnSubmit={false}
                            autoCapitalize={'none'}
                        />
                    </Item>
                    { email.error!=='' && (<Text style={styles.error}>{email.error}</Text>)}
                    <Item floatingLabel error={(address.error)?true:false}>
                    <Label style={{fontFamily: colors.fontFamily}}>Address *</Label>
                    <Input 
                    style={{fontFamily: colors.fontFamily}}
                        value={address.value}
                        onChangeText={(text)=>this.onChange('address',text)}
                        returnKeyType = { "next" }
                        onSubmitEditing={() => { this.phoneInput._root.focus() }}
                        blurOnSubmit={false}
                    />
                </Item>
                { address.error!=='' && (<Text style={styles.error}>{address.error}</Text>)}
                    <Item floatingLabel error={(phone.error)?true:false}>
                        <Label style={{fontFamily: colors.fontFamily}}>Phone Number *</Label>
                        <Input
                            getRef={input => {
                                this.phoneInput = input;
                            }}
                            style={{fontFamily: colors.fontFamily}}
                            value={phone.value}
                            onChangeText={(text)=>this.onChange('phone',text)}
                            returnKeyType = { "next" }
                            onSubmitEditing={() => this.onNext()}
                        />
                    </Item>
                    { phone.error!=='' && (<Text style={styles.error}>{phone.error}</Text>)}
                   
                </Form> 
                </View>  

                <View style={styles.nextButtonContainer}>
                    <TouchableOpacity onPress={() => this.onNext() }>
                        <View style={styles.button}>
                            <Icon style={styles.buttonColor} name={'long-arrow-alt-right'}/> 
                            <Text style={[styles.buttonText,styles.buttonColor]}>Next</Text>
                        </View>
                    </TouchableOpacity>
                </View>
              
            </ScrollView>
            </KeyboardAvoidingView>
        )
    }
}

const styles = StyleSheet.create({

    container:{
        backgroundColor:'#e1ded9',
    },
    formContainer:{
        marginBottom:20,
    },
    error:{
        color: 'red',
        fontSize:12,
        // fontStyle:'italic',
        marginLeft:10,
        fontFamily: colors.fontFamily
    },

    nextButtonContainer :{
        alignItems: 'flex-end',
        position: 'relative',
        right:10,
        top:10,
        marginBottom:10
    },
    
    buttonContainer:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical:10,
        paddingHorizontal:10
    },
      
    button :{
        alignItems : 'center',
        flexDirection: 'row',
        borderRadius:50,
        borderColor:colors.secondary,
        borderWidth:2,
        padding:8
    },
    buttonColor:{
        color : colors.secondary
    },
    buttonText : {
        paddingLeft: 10,fontFamily:colors.fontFamily
    },
    infoContainer :{
        alignItems:'center',
        justifyContent: 'center',
        flex:1,
        paddingHorizontal: 10
    },
    infoText:{ 
        fontSize: 15,
        color: "gray",
        paddingHorizontal: 12,
        fontFamily: colors.fontFamily
    }

})
