import React, { Component } from 'react'
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, ImageBackground } from 'react-native';
import { colors } from '../../styles/theme'
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from 'react-native-action-sheet';
import selectionIcon from '../../assets/selection.png'
import { v4 as uuidv4 } from 'uuid';
import Loader from '../Loader'
const MAXIMUM_IMAGES_UPLOAD=3

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as loc,
  removeOrientationListener as rol
} from 'react-native-responsive-screen';
import StepperButton from './StepperButton'

export default class Photos extends Component {
    constructor(props) {
        super(props);
        this.state = {
          images: this.props.mainState.images || [],
          imageSelectionOn: false,
          isLoading: false
        };
      }
    
      pickSingleWithCamera(cropping, mediaType='photo') {
        ImagePicker.openCamera({
          cropping: cropping,
          width: 500,
          height: 500,
          includeExif: true,
          includeBase64: true,
          mediaType,
        }).then(image => {
          this.setState({
              images:[...this.state.images,{id:uuidv4(), uri: `data:${image.mime};base64,`+ image.data, rawData: image.data, width: image.width, height: image.height, mime: image.mime}]
          })
        }).catch(e => console.log(e));
      }
    
    
      pickMultiple() {
        ImagePicker.openPicker({
          multiple: true,
          waitAnimationEnd: false,
          includeExif: true,
          forceJpg: true,
          includeBase64: true,
        }).then(images => {
          const imageArr = [...this.state.images, ...images]
            if(imageArr.length > MAXIMUM_IMAGES_UPLOAD){
                alert('You can upload upto ' + MAXIMUM_IMAGES_UPLOAD + ' photos')
                images.length = Math.abs(MAXIMUM_IMAGES_UPLOAD - this.state.images.length)
            }
            let imageList = images.map(i => {
                return {id:uuidv4(), uri: `data:${i.mime};base64,`+ i.data, rawData: i.data, width: i.width, height: i.height, mime: i.mime, isSelect: false, selectedClass: styles.imageThumbnail};
            })
            this.setState({
                images: [...this.state.images, ...imageList]
            })
        }).catch(e => console.log(e));
      }
    
      showActionSheet = () => {
        ActionSheet.showActionSheetWithOptions({
            options: ['Camera','Choose from Album', 'Cancel'],
            cancelButtonIndex: 2,
            destructiveButtonIndex: 2,
            tintColor: 'blue'
          },
          (buttonIndex) => {
            switch(buttonIndex){
                case 0:{
                    this.pickSingleWithCamera(true)
                    break
                }
                case 1:{
                    this.pickMultiple()
                    break
                }
                default:{
                    break
                }
            }
          });
      }

      actionButton = () => {
          if(this.state.imageSelectionOn){
            return (
                <TouchableOpacity onPress={this.removeImages} style={styles.button}>
                    <Text style={styles.text}>Delete Photos</Text>
                </TouchableOpacity>
            )
          }else if (this.state.images.length == MAXIMUM_IMAGES_UPLOAD){
            return (
                <TouchableOpacity disabled style={[styles.button,{backgroundColor:'#aaaaaa'}]}>
                    <Text style={styles.text}>Add Photos</Text>
                </TouchableOpacity>
            )
          }else{
            return (
                <TouchableOpacity onPress={this.showActionSheet} style={styles.button}>
                    <Text style={styles.text}>Add Photos</Text>
                </TouchableOpacity>
            )
          }
        
      }

      selectItem = (item) => {

        item.isSelect = !item.isSelect;
        item.selectedClass = item.isSelect ? styles.selectionHighlight : styles.imageThumbnail;
      
        const index = this.state.images.findIndex(
          items => item.id === items.id
        );
        this.state.images[index] = item;

        const imageSelectionOn = this.state.images.filter(item => {
          return (item.isSelect)
        });

        this.setState({
          images: this.state.images,
          imageSelectionOn : (imageSelectionOn.length > 0)
        });
        
      };

      removeImages = ( ) => {
        if(this.state.imageSelectionOn > 0){
          const images = this.state.images.filter(item => {
            return (!item.isSelect)
          });
          this.setState({
            images: images,
            imageSelectionOn : false
          });
        }
      }

      onSubmit = () => {
        this.setState({isLoading:true})
        this.props.mainState.images = this.state.images
        this.props.saveFormData(this.props.mainState, true)
      }
  
      onPrev = () => {
        const currentPosition = this.props.mainState.currentPosition - 1
        this.props.mainState.images = this.state.images
        this.props.saveFormData(this.props.mainState)
        this.props.onPageChange(currentPosition)
      }

    
      render() {

        const {currentPosition, totalPage } = this.props.mainState

        return (
        <View style={styles.container}>
        <Loader loading={this.state.isLoading} />
          <FlatList
            data={this.state.images}
            renderItem={({ item }) => (
              <View style={{ flex: 1, flexDirection: 'column', margin: 1 }}>
                <TouchableOpacity
                  onPress={() => this.selectItem(item)}
                >
                  <View style={styles.imageContainer}>
                    <ImageBackground style={[styles.imageThumbnail, item.selectedClass]} source={{uri :item.uri}} >
                    {
                      item.isSelect && (
                        <Image style={styles.selectionImg} source={selectionIcon} />
                      )
                    }
                    </ImageBackground>
                  </View>
                  
                </TouchableOpacity>   
                      
              </View>
            )}
            extraData={this.state}
            numColumns={2}
            keyExtractor={(item, index) => item.id}
          />
          <View>
            {
                this.actionButton()
            }
            <StepperButton
              totalPage={totalPage} 
              currentPage={currentPosition}
              onSubmit={this.onSubmit}
              onPrev={this.onPrev}
            />
          </View>

        </View>);
      }
}

const styles = StyleSheet.create({
    container: {
      flex:1,
      marginBottom:60,
      // height: hp('55%'),
    },
    button: {
      backgroundColor: colors.secondary,
      padding:10,
      marginBottom: 5
    },
    text: {
      color: 'white',
      fontSize: 20,
      textAlign: 'center',
      fontFamily: colors.fontFamily
    },
    imageThumbnail: {
      height: hp('25%'),
    },
    selectionHighlight: {
      opacity:0.8
    },
    selectionImg: {
      height: 30,
      width: 30,
      position:'absolute',
      right:10,
      top:10,
      zIndex:1
    },
    photo: {
      flex:1,
      position:'absolute',
      fontFamily:colors.fontFamily,
      fontSize:14,
      paddingHorizontal:12,
      top:375,
      color:'gray'
    }
});
