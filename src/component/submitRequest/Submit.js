import React, { Component } from 'react'
import { Text, View, StyleSheet, Image } from 'react-native'
import success from '../../assets/success.png'
import failed from '../../assets/failed.png'
import { colors } from '../../styles/theme'

export default class Submit extends Component {

    constructor(props){
        super(props)
    }

    
    render() {
        return (
                <View style={styles.container}>

                {
                    (this.props.success) ? 
                    <View>
                        <View style={styles.imageContainer}><Image style={styles.image} source={success} /></View>
                        <Text style={styles.text}>Your request submitted successfully</Text>
                    </View>
                    :
                    <View>
                        <View style={styles.imageContainer}><Image style={styles.image} source={failed} /></View>
                        <Text style={styles.text}>Your request submission failed</Text>
                    </View>
                    
                }

                </View>
        )
    }
}

const styles = StyleSheet.create({

    container:{
        // backgroundColor:'#e1ded9',
        justifyContent:'center',
        alignItems:'center'
    },
    imageContainer:{
        alignItems:'center'
    },
    image:{
        height:100,
        width:100,
    },
    text:{
        fontSize:22,
        textAlign:'center',
        fontFamily: colors.fontFamily
    }

})
