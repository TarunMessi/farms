import React, { Component } from 'react'
import { Dimensions, Image, Text, View, StyleSheet, TouchableOpacity, Platform, ScrollView, Alert, Modal} from 'react-native'
import Geolocation from '@react-native-community/geolocation';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import StepperButton from './StepperButton'
import Icon from "react-native-vector-icons/FontAwesome5";
import { colors } from '../../styles/theme';
import Feather from "react-native-vector-icons/Feather";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as loc,
  removeOrientationListener as rol
} from 'react-native-responsive-screen';
import Octicons from 'react-native-vector-icons/Octicons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from "react-native-vector-icons/Ionicons";
import markerIcon from '../../assets/marker.png'
import mylocationIcon from '../../assets/mylocation.png'
import { Colors } from 'react-native/Libraries/NewAppScreen';
const { height, width } = Dimensions.get('window');

const LATITUDE = 39.08782565895687;
const LONGITUDE = -77.54589593112524;
const LATITUDE_DELTA = 0.02;
const LONGITUDE_DELTA = LATITUDE_DELTA * (width / height);

const ACCURACY_TIMEOUT = {
  enableHighAccuracy: true,
  timeout: 20000,
  // maximumAge: 1000,
  // distanceFilter: 10
}
const isPortrait = () => {
  const dim = Dimensions.get('screen');
  return dim.height >= dim.width;
};

export default class Location extends Component {

  constructor(props) {
    super(props)

    this.state = {
      region: {
        Alert_Visibility:true,
        mapType:'hybrid',
        status: false,
        latitude: this.props.mainState.location.latitude || LATITUDE,
        longitude: this.props.mainState.location.longitude || LONGITUDE,
        latitudeDelta: this.props.mainState.location.latitudeDelta || LATITUDE_DELTA,
        longitudeDelta: this.props.mainState.location.longitudeDelta || LONGITUDE_DELTA,
        orientation: isPortrait() ? 'portrait' : 'landscape'
      }
    }
    this.onLayout = this.onLayout.bind(this);
    this.onRegionChange = this.onRegionChange.bind(this)
    this.getLocation = this.getLocation.bind(this)
    this.watchID = null
  }
  onLayout(e) {
    this.setState({
      orientation: isPortrait() ? 'portrait' : 'landscape'
    });
  }

  componentDidMount() {
   
   
  }
  componentWillUnmount() {
    // Geolocation.clearWatch(this.watchID);
    // Geolocation.stopObserving()
  }

  getLocation() {

    Geolocation.getCurrentPosition(
      (position) => {
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA
          }
        });
      },
      (error) => { console.log('GeolocationErr', error.message); }, ACCURACY_TIMEOUT);

    // this.watchID = Geolocation.watchPosition((position) => {
    //   console.log('watchPosition',position.coords)
    //   this.setState({
    //     region: {
    //         latitude: position.coords.latitude,
    //         longitude: position.coords.longitude,
    //         latitudeDelta: LATITUDE_DELTA,
    //         longitudeDelta: LONGITUDE_DELTA
    //     }
    //   });
    // },
    // (error) => {console.log('Geolocation-Wat-Err',error.message);alert(error.message)}, ACCURACY_TIMEOUT);

  }

  onRegionChange(region) {
    this.setState({ region: region });
  }

  onNext = () => {
    if ((this.state.region.latitude).toFixed(4) == LATITUDE && (this.state.region.longitude).toFixed(4) == LONGITUDE) {
      alert("Please pick your location")
    } else {
      const currentPosition = this.props.mainState.currentPosition + 1
      this.props.mainState.location.latitude = this.state.region.latitude
      this.props.mainState.location.latitudeDelta = this.state.region.latitudeDelta
      this.props.mainState.location.longitude = this.state.region.longitude
      this.props.mainState.location.longitudeDelta = this.state.region.longitudeDelta
      this.props.saveFormData(this.props.mainState)
      this.props.onPageChange(currentPosition)
    }
  }

  onPrev = () => {
    const currentPosition = this.props.mainState.currentPosition - 1
    this.props.mainState.location.latitude = this.state.region.latitude
    this.props.mainState.location.latitudeDelta = this.state.region.latitudeDelta
    this.props.mainState.location.longitude = this.state.region.longitude
    this.props.mainState.location.longitudeDelta = this.state.region.longitudeDelta
    this.props.saveFormData(this.props.mainState)
    this.props.onPageChange(currentPosition)
  }
  
  hideAlert =() => {
    this.setState({Alert_Visibility:false})
  }
  render() {
    const { currentPosition, totalPage } = this.props.mainState
    return (
      <ScrollView style={styles.container} onLayout={this.onLayout} showsVerticalScrollIndicator={false}>
      

        <View style={styles.mapContainer}>
        
          {
            (Platform.OS === 'android') && (
              <MapView
                provider={PROVIDER_GOOGLE}
                ref={mapView => {
                  _mapView = mapView
                }}
                mapType="hybrid"
                style={[styles.map, { marginHorizontal: this.state.orientation == 'portrait' ? 0 : 100 }]}
                initialRegion={this.state.region}
                region={this.state.region}
                showsUserLocation={true}
                showsMyLocationButton={Platform.OS === 'ios' ? true : false}
                zoomEnabled={true}
                zoomControlEnabled={false}
                onRegionChangeComplete={(e) => this.onRegionChange(e)}
                zoom={1}
              />
            )
          }
          {
            (Platform.OS === 'ios') && (
              <MapView
                provider={PROVIDER_GOOGLE}
                ref={mapView => {
                  _mapView = mapView
                }}
                mapType="hybrid"
                style={[styles.map, { marginHorizontal: this.state.orientation == 'portrait' ? 0 : 100 }]}
                initialRegion={this.state.region}
                showsUserLocation={true}
                showsMyLocationButton={Platform.OS === 'ios' ? true : false}
                zoomEnabled={true}
                zoomControlEnabled={false}
                onRegionChangeComplete={(e) => this.onRegionChange(e)}
                zoom={1}
              />
            )
          }
  
      <View style={styles.infoContainer}>
      {
       this.state.status? <View> 
      
      
       <Text style= {styles.infoText}>
       Please Select Your Location!!
       </Text>
    
       </View>
       : null
        }
    </View>

          <View style={styles.markerFixed}>
            <Image style={styles.markerIcon} source={markerIcon} />
          </View>

          {
            Platform.OS != 'ios' && (

              <TouchableOpacity
                style={[styles.myLocationButton,{ marginHorizontal: this.state.orientation == 'portrait' ? 0 : 100 }]}
                onPress={() => {
                  if (Platform.OS === 'android') {
                    RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({ interval: 10000, fastInterval: 5000 })
                      .then(data => {
                        this.getLocation()
                      }).catch(err => {
                        console.log('android', err)
                      });
                  } else {
                    this.getLocation();
                  }
                }}
              >
                <Image style={styles.locationIcon} source={mylocationIcon} />
              </TouchableOpacity>
            )
          }
        </View>

        <View style={styles.bottomView}>
        <TouchableOpacity onPress={() => this.onPrev() }>
            <View style={styles.button}>
                <Icon style={styles.buttonColor} name={'long-arrow-alt-left'}/> 
                <Text style={[styles.buttonText,styles.buttonColor]}>Prev</Text>
            </View>
          </TouchableOpacity>
        
          <TouchableOpacity onPress={() => this.onNext() }>
              <View style={styles.button}>
                  <Icon style={styles.buttonColor} name={'long-arrow-alt-right'}/> 
                  <Text style={[styles.buttonText,styles.buttonColor]}>Next</Text>
              </View>
            </TouchableOpacity>

        </View>

      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // height: hp('50%'),
    flex: 1,
  },
  map: {
    height: hp('60%'),
    // position:'absolute',
    // top:0,
    // left:0,
    // right:0,
    // bottom:0,
  },
  markerFixed: {
    left: '50%',
    marginLeft: -24,
    marginTop: -48,
    position: 'absolute',
    top: '50%'
  },
  markerFixeds: {
    left: '72%',
    position: 'absolute',
    top: '77%'
  },
  markerIcon: {
    height: 48,
    width: 48
  },
  locationIcon: {
    height: 30,
    width: 30
  },
  myLocationButton: {
    backgroundColor: 'white',
    position: 'absolute',
    bottom: 20,
    right: 10,
    padding: 15,
    elevation: 3,
    alignItems: 'center',
    alignSelf: 'flex-end',
    justifyContent: 'center',
    borderRadius: 50
  },
  bottomView: {
    justifyContent:'space-between',
    flexDirection:'row',alignItems:'center',paddingVertical:20
  },
button :{
  alignItems : 'center',
  flexDirection: 'row',
  borderRadius:50,
  borderColor:colors.secondary,
  borderWidth:2,
  padding:8,paddingHorizontal:20,
},
buttonColor:{
  color : colors.secondary
},
buttonText : {
  paddingLeft: 10,
},
infoContainer :{
  borderWidth:0.5,
  borderColor:'#fff',
  borderRadius:10,
  top:150,
  left:100,
  position:'absolute'
},
infoText:{ 
  fontSize: 12,
  color: "gray",
  fontFamily: colors.italicFontFamily,
},
Alert_Main_View: {
  alignItems:'center',
  justifyContent:'center',
  backgroundColor:colors.tertiary,
  height:"20%",
  width:"62%",
  borderWidth:0.5,
  borderRadius:10,
  borderColor:colors.tertiary,
  marginTop:40
},
okay: {
  fontSize:15,
  fontWeight:'500',
  color:colors.iconColor,
  borderRadius:20,
  paddingLeft:150,paddingTop:10
  
},
dot: {
  fontSize:15,
  color:'#000',
  paddingHorizontal:2,
  
},
});