import React, { Component } from 'react'
import { Text, View, StyleSheet, ScrollView } from 'react-native'
import { Form, Item, Label, Textarea } from 'native-base';
import StepperButton from './StepperButton';
import { KeyboardAccessoryNavigation } from 'react-native-keyboard-accessory';
import { colors } from '../../styles/theme';

export default class Description extends Component {

    constructor(props) {
        super(props)
        this.state = {
            description: { value: this.props.mainState.description, error: '' }
        }
    }

    onNext = async () => {
        if (await this.isValid()) {
            const currentPosition = this.props.mainState.currentPosition + 1
            this.props.mainState.description = this.state.description.value
            this.props.saveFormData(this.props.mainState)
            this.props.onPageChange(currentPosition)
        }
    }

    onPrev = async () => {
        if (await this.isValid()) {
            const currentPosition = this.props.mainState.currentPosition - 1
            this.props.mainState.description = this.state.description.value
            this.props.saveFormData(this.props.mainState)
            this.props.onPageChange(currentPosition)
        }
    }

    isValid = () => {
        const { description } = this.state
        return new Promise(resolve => {
            this.validateDescription(description.value.trim())
            this.setState({
                ...this.state
            }, () => {
                if (description.error == '') {
                    resolve(true)
                } else {
                    resolve(false)
                }
            });

        }).catch((err) => {
            console.log(err)
        })
    }

    validateDescription = (value) => {
        if (value.trim() == '') { //!/^\w+( +\w+)*$/.test(value)
            this.state['description'].error = 'Description required'
            return false
        } else {
            this.state['description'].error = ''
            return true
        }
    }

    onChange(name, value) {

        if (name == 'description') {
            this.validateDescription(value)
        } else {
            this.state[name].error = '';
        }
        this.state[name].value = value;

        this.setState({
            ...this.state
        });
    }

    render() {
        const { currentPosition, totalPage } = this.props.mainState
        return (
            <View style={styles.mainContainer}>
                <ScrollView style={styles.mainContainer}>
                    <View style={styles.container}>
                        <Form style={styles.formContainer}>
                            <Label style={styles.description}>Description * {this.state.description.error !== '' && (<Text style={styles.error}>{this.state.description.error}</Text>)}</Label>
                            <Textarea
                                rowSpan={5}
                                placeholder=""
                                value={this.state.description.value}
                                onChangeText={(text) => this.onChange('description', text)}
                                returnKeyType={"next"}
                                autoCorrect={false}
                                style={{fontFamily: colors.fontFamily}}
                            />
                        </Form>
                    </View>
                    <View style={styles.buttonContainer}>
                        <StepperButton
                            totalPage={totalPage}
                            currentPage={currentPosition}
                            onNext={this.onNext}
                            onPrev={this.onPrev} />
                    </View>
                </ScrollView>
                <KeyboardAccessoryNavigation
                    androidAdjustResize={true}
                    nextHidden={true}
                    previousHidden={true} />
            </View>
        )
    }
}

const styles = StyleSheet.create({

    container: {
        backgroundColor: '#e1ded9',
    },
    formContainer: {
        marginBottom: 20,
    },
    description: {
        padding: 10,
        color: 'gray',
        fontFamily: colors.fontFamily
    },
    error: {
        color: 'red',
        fontSize: 12,
        // fontStyle: 'italic',
        marginLeft: 10,
        fontFamily: colors.italicFontFamily
    },
    buttonContainer: {
        marginBottom: 50
    },
    mainContainer: {
        flex: 1
    }

})