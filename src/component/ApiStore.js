import AsyncStorage from '@react-native-async-storage/async-storage';
const MOKO_URL ='https://moko-notification-server.herokuapp.com'
const MOKO_API_KEY =''
const MOKO_RELAY_KEY='bc885d76883eefc62004f3c21074270938482f509b6f35925bebd8466cf11ee31334f51deafc1f43339452cdc7f3f503172c4dad15cf649f6b3255750fcef396'
const MOKO_RELAY_DOMAIN=''
const MOKO_RELAY_ROUTE='e6f030c61be8a051cc2f1b2aee61e2050d47a12f23db6c4aa70bae47fc907f647e845586e06513ddd7b119a565128b650d47c4a3cfc9520cb7773059db46262e'
const ONE_SIGNAL_APP_ID='8a4e71d8-cfcb-409b-8f01-fb1b6737da0d'
const WORDPRESS_URL='http://localhost/TavistockFarms'
const getNotificationsHistory = () => {

    return new Promise(async resolve => {
        const data = { apiKey: MOKO_API_KEY, time: Date.now() };
        try {
            let response = await fetch(
                `${MOKO_URL}/notifications?ts=${encodeURIComponent(data.time)}`,
                {
                    method: 'get',
                    headers: new Headers({
                        'X-Api-Key': data.apiKey,
                        'Content-Type': 'application/json'
                    }),
                }
            );
            let responseJson = await response.json();
            resolve(responseJson.notifications);
        } catch (error) {
            resolve([])
        }
    }).catch((err) => {
        console.log(err)
    })

}

const getNewsletter = () => {

    return new Promise(async resolve => {
        try {
            let response = await fetch(
                'http://app.vistafilare.org/wp-json/moko/v1/newsletters',
                {
                    headers: {
                        "Content-Type": "application/json",
                        'Accept': 'application/json'
                    },
                }
            );
            let responseJson = await response.json();
            resolve(responseJson.newsletters);
        } catch (error) {
            console.error(error);
        }
    }).catch((err) => {
        console.log(err)
    })
}

const getDocuments = () => {

    return new Promise(async resolve => {
        try {
            let response = await fetch(
                'http://localhost/TavistockFarms/wp-json/moko/v1/documents',
                {
                    headers: {
                        "Content-Type": "application/json",
                        'Accept': 'application/json'
                    },
                }
            );
            let responseJson = await response.json();
            resolve(responseJson.documents);
        } catch (error) {
            console.error(error);
        }
    }).catch((err) => {
        console.log(err)
    })

}

const getRealestate = () => {

    return new Promise(async resolve => {
        try {
            let response = await fetch(
                'http://app.vistafilare.org' + '/moko-real-estate/',
                {
                    headers: {
                        "Content-Type": "application/json",
                        'Accept': 'application/json'
                    },
                });
            let htmlString = await response.text();
            resolve(htmlString);
        } catch (error) {
            console.error(error);
        }
    }).catch((err) => {
        console.log(err)
    })

}

const getCalendarEvents = (startFrom) => {

    return new Promise(async resolve => {
        try {
            let response = await fetch(
                `${WORDPRESS_URL}/wp-json/tribe/events/v1/events?start_date=${startFrom}&page=1&per_page=1000`,
                {
                    headers: {
                        "Content-Type": "application/json",
                        'Accept': 'application/json'
                    },
                }
            );
            let responseJson = await response.json();
            resolve(responseJson.events);
        } catch (error) {
            console.error(error);
        }
    }).catch((err) => {
        console.log(err)
    })
}

const submitRequest = (data) => {

    var postData = {
        secret_key: "" + MOKO_RELAY_KEY,
        request_type: 'work',
        name: data.name,
        description: data.description,
        email: data.email,
        phone_number: data.phone,
        street_address: data.address,
        images: data.images,
        latitude: '',
        longitude: ''
    };
    return new Promise(async resolve => {
        try {
            let response = await fetch(
                `${MOKO_RELAY_DOMAIN}${MOKO_RELAY_ROUTE}`,
                {
                    method: "POST",
                    headers: {
                        "Accept": "application/json",
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify(postData)
                }
            );
            if (response.status >= 200 && response.status < 300) {
                let responseJson = await response.json();
                resolve({ status: 1, data: responseJson });
            } else {
                resolve({ status: 0, data: '' })
            }
        } catch (error) {
            resolve({ status: 0, data: '' })
        }
    }).catch((err) => {
        console.log(err)
    })

}

const syncNotificationTags = async (OneSignal) => {

    // AsyncStorage.removeItem('notificationTags')
    try {
        var defaultTags = [
            {
                id: 1,
                tag: 'emergency_alerts',
                name: 'Emergency Alerts',
                default: true
            }, 
            {
                id: 2,
                tag: 'general',
                name: 'General',
                default: true
            }, 
            {
                id: 3,
                tag: 'road_notices',
                name: 'Road Notices',
                default: true
            },
            {
                id: 4,
                tag: 'pool_alerts',
                name: 'Pool Alerts',
                default: true
            }, 
            {
                id: 5,
                tag: 'parking',
                name: 'Parking',
                default: true
            }, 
             {
                id: 6,
                tag: 'trash_recycling',
                name: 'Trash & Recycling',
                default: true
            }, 
            {
                id: 7,
                tag: 'community_events',
                name: 'Community Events',
                default: true
            }, 
            {
                id: 8,
                tag: 'facility_alerts',
                name: 'Facility Alerts ',
                default: true
            }, 
        ];
        var keyValue = {}
        var tags = await AsyncStorage.getItem('notificationTags');
        if (tags != null) {
            let parsedTag = JSON.parse(tags)
            for (var i = 0; i < defaultTags.length; i++) {
                if (defaultTags[i].id == parsedTag[i].id) {
                    defaultTags[i].default = parsedTag[i].default
                    if (parsedTag[i].default) {
                        keyValue[parsedTag[i].tag] = parsedTag[i].tag
                    } else {
                        OneSignal.deleteTag(parsedTag[i].tag);
                    }
                }
            }
            if (Object.keys(keyValue) > 0) {
                console.log('keyValue', JSON.stringify(keyValue))
                OneSignal.sendTags(keyValue)
            }

        } else {
            for (var i = 0; i < defaultTags.length; i++) {
                keyValue[defaultTags[i].tag] = defaultTags[i].tag
            }
            OneSignal.sendTags(keyValue)
        }

        AsyncStorage.setItem('notificationTags', JSON.stringify(defaultTags))
            .then(() => {
                // console.log('filterTag',JSON.stringify(defaultTags,null,2))
            })
            .catch(e => { console.log('error', e) })

    } catch (error) {
        console.error(error);
    }
}
const getContactNumbers = () => {

    return new Promise(async resolve => {
        try {
            let response = await fetch(
                'http://localhost/TavistockFarms/wp-json/moko/v1/contact_numbers',
                {
                    headers: {
                        "Content-Type": "application/json",
                        'Accept': 'application/json'
                    },
                }
            );
            let responseJson = await response.json();
            resolve(responseJson.contact_numbers);
        } catch (error) {
            console.error(error);
        }
    }).catch((err) => {
        console.log(err)
    })
}
const getEmailAddress = () => {

    return new Promise(async resolve => {
        try {
            let response = await fetch(
                'http://localhost/PMPHOA/wp-json/moko/v1/email_address',
                {
                    headers: {
                        "Content-Type": "application/json",
                        'Accept': 'application/json'
                    },
                }
            );
            let responseJson = await response.json();
            resolve(responseJson.contact_numbers);
        } catch (error) {
            console.error(error);
        }
    }).catch((err) => {
        console.log(err)
    })
}
const getAnnouncements = () => {

    return new Promise(async resolve => {
        try {
            let response = await fetch(
                'http://localhost/TavistockFarms/wp-json/moko/v1/announcements',
                {
                    headers: {
                        "Content-Type": "application/json",
                        'Accept': 'application/json'
                    },
                }
            );
            let responseJson = await response.json();
            resolve(responseJson.announcements);
        } catch (error) {
            console.error(error);
        }
    }).catch((err) => {
        console.log(err)
    })
}
const getImages = () => {

    return new Promise(async resolve => {
        try {
            let response = await fetch(
                'http://localhost/TavistockFarms/wp-json/moko/v1/carouselimg',
                {
                    headers: {
                        "Content-Type": "application/json",
                        'Accept': 'application/json'
                    },
                }
            );
            let responseJson = await response.json();
            resolve(responseJson);
        } catch (error) {
            console.error(error);
        }
    }).catch((err) => {
        console.log(err)
    })
}


export {
    getNotificationsHistory,
    getNewsletter,
    getDocuments,
    getAnnouncements,
    getRealestate,
    getCalendarEvents,
    submitRequest,
    syncNotificationTags,
    getContactNumbers,
    getImages,
    getEmailAddress,
    ONE_SIGNAL_APP_ID
}

