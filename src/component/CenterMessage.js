import React from 'react'
import {
    View,
    Text,
    StyleSheet
} from 'react-native'

import { colors } from '../styles/theme'

export default ({message}) => (

    <View style={ styles.container }>
        <View styel={ styles.textContainer}>
            <Text style={ styles.text }>{message}</Text>
        </View>
    </View>
)

const styles = StyleSheet.create({
    container : {
        padding :10,
        borderBottomWidth: 2,
        borderBottomColor : colors.primary
    },
    textContainer :{
        position: "absolute",
        left : 0,
        bottom : 10
    },
    text:{
        textAlign: 'center',
    }
})