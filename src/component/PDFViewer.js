import React, { Component } from 'react'
import { View, StatusBar, TouchableOpacity, StyleSheet, Dimensions, PermissionsAndroid, Alert, Platform } from 'react-native';
import { colors } from '../styles/theme'
import Icon from "react-native-vector-icons/FontAwesome5";
import Pdf from 'react-native-pdf';
import stylesTheme from '../styles/StyleTheme';
import RNFetchBlob from 'rn-fetch-blob';
import Loader from './Loader';
import AntIcon from "react-native-vector-icons/AntDesign";

const headerTitleStyles = {
    color: '#fff',
    alignSelf: 'center',
    textAlign: 'center'
}
export default class DetailNewsletter extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
        }
    }

    checkPermission = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                //Once user grant the permission start downloading
                this.downloadImageAndroid();
            } else {
                //If permission denied then show alert 'Storage Permission Not Granted'
                alert('Storage Permission Not Granted. Please enable storage permission and download the document.');
            }
        } catch (err) {
            //To handle permission related issue
            console.warn(err);
        }
    };

    downloadImageAndroid = () => {
        this.setState({
            isLoading: true
        })
        const data = this.props.route.params.data;

        const { config, fs } = RNFetchBlob;
        let PictureDir = fs.dirs.DownloadDir;
        let options = {
            fileCache: true,
            addAndroidDownloads: {
                useDownloadManager: true,
                notification: true,
                path:
                    PictureDir + `/${data.downloadTitle}.pdf`,
                description: 'Downloading file',
            },
        };
        config(options)
            .fetch('GET', data.description)
            .then(res => {
                Alert.alert("", "Downloaded Successfully.",
                    [
                        {
                            text: 'OK', onPress: () => {
                                this.setState({
                                    isLoading: false
                                })
                            }
                        }
                    ],
                    { cancelable: false }
                )
            });
    };
    downloadImageIOS = () => {
        this.setState({
            isLoading: true
        })
        const data = this.props.route.params.data;
        let dirs = RNFetchBlob.fs.dirs.DocumentDir;
        RNFetchBlob.config({
            fileCache: true,
            path: dirs + `/${data.downloadTitle}.pdf`,
        })
            .fetch(
                'GET',
                data.description,
                {
                    //some headers ..
                },
            )
            .then(res => {
                this.setState({
                    isLoading: false
                })
                if (Platform.OS === "ios") {
                    RNFetchBlob.ios.previewDocument(res.data);
                }
            });
    };

    render() {
        const data = this.props.route.params.data;
        this.props.navigation.setOptions({
            title: `${data.title}`,
            headerRight: () => <TouchableOpacity onPress={() => {
                if (Platform.OS === 'ios') {
                    this.downloadImageIOS()
                } else {
                    this.checkPermission()
                }
            }}>
                <View style={{ paddingHorizontal: 15}}>
                    <AntIcon name="download" style={stylesTheme.headerLeftIcon} />
                </View>
            </TouchableOpacity>,
            headerLeft: () =>
                <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                    <View style={{ paddingHorizontal: 10 }}>
                        <Icon name="arrow-left" style={stylesTheme.headerLeftIcon} />
                    </View>
                </TouchableOpacity>,
            headerTitleAlign: 'center'
        })
        return (
            <View style={styles.container}>
            <Loader loading={this.state.isLoading} />
                <Pdf
                    ref={(pdf) => { this.pdf = pdf; }}
                    source={{ uri: data.description, cache: true }}
                    onLoadComplete={(numberOfPages, filePath) => {
                    }}
                    onPageChanged={(page, numberOfPages) => {
                    }}
                    onError={(error) => {
                        alert('There is a problem loading the document');
                        this.props.navigation.goBack();
                    }}
                    style={styles.pdf} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    pdf: {
        flex: 1,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
    }
});


