import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  ActivityIndicator,
  SectionList,
  TouchableOpacity
} from 'react-native';
import unescape from 'lodash/unescape';

import CenterMessage from './component/CenterMessage'
import { getAnnouncements } from './component/ApiStore'
import moment from 'moment';
import { colors } from './styles/theme';
import Icon from "react-native-vector-icons/FontAwesome5";
import AsyncStorage from '@react-native-async-storage/async-storage';
const STORAGE_KEY = 'announcements';
import stylesTheme from './styles/StyleTheme';



export default class Announcements extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      announcements: []
    }
  }

  async componentDidMount() {
    StatusBar.setBarStyle('light-content', true);
    if (Platform.OS === 'android') {
      StatusBar.setBackgroundColor(colors.primary);
    }
    this._isMounted = true;
    var announcementsStore = await AsyncStorage.getItem(STORAGE_KEY);
    if (announcementsStore != null) {
      if (this._isMounted) {
        this.setState({
          isLoading: false,
          announcements: JSON.parse(announcementsStore) || []
        })
      }
      this.syncAnnc()
    } else {
      this.syncAnnc()
    }
  }

  async syncAnnc() {
    const announcementsData = await getAnnouncements()
    const announcements = await this.announcementsArrayToMap(announcementsData)

    if (this._isMounted) {
      AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(announcements))
        .then(() => {
          this.setState({
            isLoading: false,
            announcements: announcements
          })
        })
        .catch(e => { console.log('error', e) })
    }
  }

  announcementsArrayToMap = (announcements) => {
    return new Promise(resolve => {
      const data = announcements.reduce((acc, item) => {
        const deliveredAt = moment(item.post_date).format('MMM YYYY')
        const foundIndex = acc.findIndex(element => element.title === deliveredAt);
        if (foundIndex === -1) {
          return [
            ...acc,
            {
              title: deliveredAt,
              data: [item],
            },
          ];
        }
        acc[foundIndex].data = [...acc[foundIndex].data, item];
        return acc;
      }, []);
      resolve(data)
    })
  }

  renderRow = (item) => {
    const deliveredAt = moment(item.post_date).format('M/DD')
    return (
      <TouchableOpacity onPress={() => (item.post_content != '') ? this.props.navigation.navigate('detailAnnouncements', {
        data: {
          title: item.post_title,
          description: item.post_content
        }
      }) : false}>
        <View style={styles.SectionListItem}>
          {<Icon style={{ width: '5%', alignSelf: 'center' }} name={'calendar'} />}
          <Text style={{ width: '15%', alignSelf: 'center' }}> {deliveredAt}</Text>
          <Text style={{ width: '80%', alignSelf: 'center', justifyContent: 'center' }}> {unescape(item.post_title)} </Text>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
  
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      )
    }

    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" backgroundColor={colors.primary} />
        {
          !this.state.announcements.length && <CenterMessage message="No more news" />
        }

        <SectionList
          renderSectionHeader={({ section }) => <Text style={styles.SectionHeader}> {section.title} </Text>}
          renderItem={({ item }) => this.renderRow(item)}
          keyExtractor={(item, index) => index}
          sections={this.state.announcements}
          ItemSeparatorComponent={() => (
            <View style={{ height: 0.5, width: '100%', backgroundColor: '#b1a99f' }} />
          )}
        />

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: colors.tertiary,
  },
  SectionHeader: {
    backgroundColor: colors.primary,
    textAlign: 'center',
    fontSize: 20,
    padding: 5,
    color: '#fff',
    fontStyle: 'italic'
  },
  SectionListItem: {
    fontSize: 14,
    padding: 10,
    color: '#000',
    backgroundColor: colors.listBackground,
    flex: 1,
    flexDirection: 'row'
  }
})

