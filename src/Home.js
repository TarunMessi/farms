import React, { Component } from 'react'
import { StyleSheet, Text, View, FlatList, Dimensions, Image, TouchableWithoutFeedback, StatusBar, Linking, Platform } from 'react-native';
// import { SliderBox } from "react-native-image-slider-box";
import Icon from "react-native-vector-icons/FontAwesome5";
import Entypo from "react-native-vector-icons/Entypo";
import SplashScreen from 'react-native-splash-screen';
 import OneSignal from 'react-native-onesignal';
 import { syncNotificationTags, getImages,ONE_SIGNAL_APP_ID } from './component/ApiStore';
import { colors } from './styles/theme';
import Foundation from "react-native-vector-icons/Foundation";
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {OpacityDotsLoader} from 'react-native-indicator';
const data = [
  {
    key: 'News',
    icon: 'news',
    screen: 'announcements'
  },
  {
    key: 'Alerts',
    icon: 'flag',
    screen: 'notifications'
  },
  {
    key: 'Calendar',
    icon: 'calendar-alt',
    screen: 'calender'
  },
  {
    key: 'Documents',
    icon: 'folder-open',
    screen: 'documents'
  },
  
  {
    key: 'Submit A Request',
    icon: 'map-marker-alt',
    screen: 'submitRequest'
  },
  {
    key: 'Useful Numbers',
    icon: 'phone',
    screen: 'contactInfo'
  },
  {
    key: 'Settings',
    icon: 'cog',
    screen: 'settings'
  },
  {
    key: 'Digital PoolPass',
    icon: 'id-card',
    screen: 'poolPass'
  },
    ];
const formatData = (data, numColumns) => {
  const numberOfFullRows = Math.floor(data.length / numColumns);

  let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
  while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
    data.push({ key: `blank-${numberOfElementsLastRow}`, empty: true });
    numberOfElementsLastRow++;
  }

  return data;
};

const numColumns = 4;
const STORAGE_KEY = 'homeImages'

const isPortrait = () => {
  const dim = Dimensions.get('screen');
  return dim.height >= dim.width;
};
export default class Home extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      homeImages: [require('./assets/PMPP.jpeg')],
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
      orientation: isPortrait() ? 'portrait' : 'landscape'
    }
    this.onLayout = this.onLayout.bind(this);
    // OneSignal.init(ONE_SIGNAL_APP_ID, { kOSSettingsKeyAutoPrompt: true, kOSSettingsKeyInFocusDisplayOption: 0 });
    // OneSignal.addEventListener('received', this.onReceived);
    // OneSignal.addEventListener('opened', this.onOpened);
    // OneSignal.inFocusDisplaying(2) // Disable foreground notification alert

    //Sync notification tags with one signal
    syncNotificationTags(OneSignal);
    SplashScreen.hide();
  }
  async componentDidMount() {
    if (Platform.OS === 'android') {
      StatusBar.setBackgroundColor('#61afe4');
      StatusBar.setBarStyle('light-content', true);
    }
    this._isMounted = true
  //   var imageData = await AsyncStorage.getItem(STORAGE_KEY);
  //   if (imageData != null) {
  //     if (this._isMounted) {
  //       this.setState({
  //         homeImages: JSON.parse(imageData)
  //       })
  //     }
  //     this.syncImages()
  //   } else {
  //     this.syncImages()
  //   }
  }
  // async syncImages() {
  //   const ImagesData = await getImages()
  //   if (this._isMounted) {
  //     AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(ImagesData))
  //       .then(() => {
  //         this.setState({
  //           homeImages: ImagesData
  //         })
  //       })
  //       .catch(e => { console.log('error', e) })
  //   }
  // }
  onLayout(e) {
    this.setState({
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
      orientation: isPortrait() ? 'portrait' : 'landscape'
    });
  }
  componentWillUnmount() {
    this._isMounted = false
    // OneSignal.removeEventListener('received', this.onReceived);
    // OneSignal.removeEventListener('opened', this.onOpened);
    // // this._listener.remove()
  }
  // onReceived(notification) {
  //   console.log("Notification received: ", notification);
  // }
  // onOpened = (notification) => {
  //   this.props.navigation.navigate('notifications')
  // }

  onReceived(notification) {
    // if(Platform.OS === 'ios'){
    //     Alert.alert(
    //         'Notification',
    //         notification.payload.body,
    //         [
    //           {text: 'OK', onPress: () => {return true }},
    //         ],
    //         {cancelable: false},
    //     );
    // }
  }
  static navigationOptions = { header: null };

  renderItem = ({ item, index }) => {
    if (item.empty === true) {
      return <View style={[styles.item, styles.itemInvisible]} />;
    }
    itemClicked = (item) => {
      console.log(item.key);
    }
    const screen = item.screen
    if (screen == 'payDues') {
      return (
          <TouchableWithoutFeedback onPress={() => {
              Linking.openURL(`https://www.bbt.com/small-business/payments-and-processing/association-services/homeowner-payments-through-association-services.html`);
          }}>
              <View style={[styles.item, { height: this.state.height / 6 }]}>
                  <Foundation style={styles.dollarIcon} name={item.icon} />
                  <Text style={styles.itemText}>{item.key}</Text>
              </View>
          </TouchableWithoutFeedback>
      );
  }
   else if(screen=='announcements') {
      return (
        <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate(screen)}>
          <View style={[styles.item, { height: this.state.height / 6 }]}>
            <Entypo style={styles.itemIcon} name={item.icon} />
            <Text style={styles.itemText}>{item.key}</Text>
          </View>
        </TouchableWithoutFeedback>
      );

   } else if (screen == 'more') {
      return (
       <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate(screen)}>
       <View style={[{backgroundColor: colors.primary,
       alignItems:'center',paddingTop:55,
        flex:1,
       borderWidth: 1,
      borderColor: colors.separatorLine,}, { height: this.state.height / 6 }]}>
      <OpacityDotsLoader size={10} color={colors.tertiary} dotsNumber={3} betweenSpace={5} speed={500}/>
      <Text style={{color: '#fff',fontSize: 12,fontFamily:colors.fontFamily, paddingTop:10}}>{item.key}</Text>
      </View>
     </TouchableWithoutFeedback>
     )
    } else {
       return (
   <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate(screen)}>
   <View style={[styles.item, { height: this.state.height / 6 }]}>
   <Icon style={styles.itemIcon} name={item.icon} />
   <Text style={styles.itemText}>{item.key}</Text>
   </View>
   </TouchableWithoutFeedback>
     );
     }
    };

  render() {
    return (
      <View style={styles.containerMain} onLayout={this.onLayout}>
      <StatusBar barStyle="light-content" backgroundColor={'#61afe4'} />
      <Image source={require('./assets/mains.jpg')} style={{flex:1,width:"100%"}}/>
      <Image source={require('./assets/logoFarm.png')} style={{position:'absolute',width:"100%",height:"9%",top:60}}/>
      <View style={styles.bottomView}>
        <FlatList
          data={formatData(data, numColumns)}
          renderItem={this.renderItem}
          numColumns={numColumns}
          scrollEnabled={false}
          extraData={this.state}
        />
      </View>
    </View>
    )
  }
}

const styles = StyleSheet.create({

containerMain: {
  flex: 1,
  backgroundColor: colors.primary,
},
headerView: {
  flex: 1,
  alignItems: 'center',
  backgroundColor: '#000'
},
bottomView: {
  width: '100%',
  position: 'relative', //Here is the trick
  bottom: 0, //Here is the trick
},
item: {
  backgroundColor: colors.primary,
  alignItems: 'center',
  justifyContent: 'center',
  flex: 1,
  borderWidth: 1,
  borderColor: colors.separatorLine,
},
itemInvisible: {
  backgroundColor: 'transparent',
},
itemText: {
  color: '#fff',
  textAlign: 'center',
  fontSize: 12,
  paddingTop: 5,
  paddingHorizontal: 5
},
itemIcon: {
  color: colors.separatorLine,
  fontSize: 30,
},
dollarIcon:{
  color: colors.separatorLine,
  fontSize: 40
},
});

