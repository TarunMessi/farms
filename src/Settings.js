import React, { Component } from 'react'
import { View, StyleSheet, StatusBar, ScrollView, TouchableOpacity, Alert } from 'react-native'
import { colors } from './styles/theme'
import { ListItem, Text, Left, Body, Right, Switch } from 'native-base';
import AsyncStorage from '@react-native-async-storage/async-storage';
import OneSignal from 'react-native-onesignal';
import Icon from "react-native-vector-icons/FontAwesome5";
import stylesTheme from './styles/StyleTheme';

const STORAGE_KEY = 'notificationTags'
const defaultTags = [
    {
        id: 1,
        tag: 'emergency_alerts',
        name: 'Emergency Alerts',
        default: true
    }, 
    {
        id: 2,
        tag: 'general',
        name: 'General',
        default: true
    }, 
    {
        id: 3,
        tag: 'road_notices',
        name: 'Road Notices',
        default: true
    },
    {
        id: 4,
        tag: 'pool_alerts',
        name: 'Pool Alerts',
        default: true
    }, 
    {
        id: 5,
        tag: 'parking',
        name: 'Parking',
        default: true
    }, 
     {
        id: 6,
        tag: 'trash_recycling',
        name: 'Trash & Recycling',
        default: true
    }, 
    {
        id: 7,
        tag: 'community_events',
        name: 'Community Events',
        default: true
    }, 
    {
        id: 8,
        tag: 'facility_alerts',
        name: 'Facility Alerts ',
        default: true
    }, 
    ];

export default class Settings extends Component {

    constructor(props) {
        super(props);

        this.state = {
            tags: defaultTags
        }
    }

    async componentDidMount() {
        if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(colors.primary);
            StatusBar.setBarStyle('light-content', true);
          }
        let tags = await AsyncStorage.getItem(STORAGE_KEY)
        this.setState({
            tags: JSON.parse(tags)
        })
    }

    onChange = (value) => {
        this.state.tags.map(item => {
            if (item.id == value) {
                if (item.default) {
                    item.default = false
                    OneSignal.deleteTag(item.tag);
                } else {
                    item.default = true
                    OneSignal.sendTag(item.tag, item.tag)
                }
            }
        })
        this.setState({
            ...this.state
        }, () => {
            AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(this.state.tags))
                .then(() => {
                    // console.log('filterTag',JSON.stringify(this.state.tags,null,2))
                })
                .catch(e => { console.log('error', e) })
        })
    }

    onPressLink = (event) => {

        this.props.navigation.navigate('webPage', {
            title: (event == 'privacy') ? 'Privacy Policy' : 'Terms Of Use',
            id: event
        })
    }

    logoutAlert = () => {
        Alert.alert(
            'Are you Sure?',
            'You want to Logout.',
            [{
                text: 'OK', onPress: () => {
                    this.logout()
                }
            },
            { text: 'Cancel' },
            ],
            { cancelable: false }
        )
    }
    async logout() {
        try {
            await AsyncStorage.clear()
            this.props.navigation.navigate('login')
        } catch (error) {
            console.log(`The error is: ${error}`)
        }
    }
    render() {
        
        return (
            <View style={styles.container}>
                <ScrollView>
                    <StatusBar barStyle="light-content" backgroundColor={colors.primary} />
                    <View style={styles.bottomSpace}>
                        <Text style={styles.headers}>Alert Tags</Text>
                    </View>
                    <Text style={[styles.bottomSpace, styles.hint]}> * Select the types of alert you would like to receive</Text>

                    {
                        this.state.tags.map((item) => {
                            return (
                                <ListItem key={item.id}>
                                    <Body>
                                        <Text style={{fontFamily: colors.fontFamily}}>{item.name}</Text>
                                    </Body>
                                    <Right>
                                        <Switch onChange={() => this.onChange(item.id)} value={item.default} />
                                    </Right>
                                </ListItem>
                            )
                        })
                    }
                    <View style={styles.bottomSpace}>
                        <Text style={styles.header}>Legal</Text>
                    </View>
                    <ListItem key="privacy" onPress={() => this.onPressLink('privacy')}>
                        <Body>
                            <Text style={{fontFamily: colors.fontFamily}}>Privacy Policy</Text>
                        </Body>
                        <Right>
                            <Icon name="chevron-right" style={{ color: '#c8c8c8' }} />
                        </Right>
                    </ListItem>
                    <ListItem key="terms" onPress={() => this.onPressLink('terms')}>
                        <Body>
                            <Text style={{fontFamily: colors.fontFamily}}>Terms of Use</Text>
                        </Body>
                        <Right>
                            <Icon name="chevron-right" style={{ color: '#c8c8c8' }} />
                        </Right>
                    </ListItem>
                   
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.tertiary,
    },
    header: {
        backgroundColor: colors.primary,
        textAlign: 'center',
        fontSize: 20,
        padding: 5,
        color: '#fff',
        // fontStyle: 'italic',
        fontFamily: colors.italicFontFamily
    },
    headers: {
        backgroundColor: colors.tertiary,
        textAlign: 'center',
        fontSize: 20,
        padding: 5,
        color: '#000',
        // fontStyle: 'italic',
        fontWeight:'700',
        fontFamily: colors.italicFontFamily
    },
    bottomSpace: {
        marginBottom: 10
    },
    hint: {
        fontSize: 14,
        // fontStyle: 'italic',
        padding: 10,
        fontFamily: colors.italicFontFamily
    }

});

