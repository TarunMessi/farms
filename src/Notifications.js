import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  ActivityIndicator,
  SectionList,
  TouchableOpacity
} from 'react-native';

import CenterMessage from './component/CenterMessage'
import { getNotificationsHistory } from './component/ApiStore'
import moment from 'moment';
import { colors } from './styles/theme';
import Icon from "react-native-vector-icons/FontAwesome5";
import AsyncStorage from '@react-native-async-storage/async-storage';
const STORAGE_KEY = 'notifications';
import stylesTheme from './styles/StyleTheme';
import Hyperlink from 'react-native-hyperlink';

class Notifications extends Component {

  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      notifications: []
    }
  }

  async componentDidMount() {
    StatusBar.setBarStyle('light-content', true);
    if (Platform.OS === 'android') {
      StatusBar.setBackgroundColor(colors.primary);
    }
    this._isMounted = true;
    //this._listener = this.props.navigation.addListener('didFocus', async () => {
    var notificationStore = await AsyncStorage.getItem(STORAGE_KEY);
    if (notificationStore != null) {
      if (this._isMounted) {
        this.setState({
          isLoading: false,
          notifications: JSON.parse(notificationStore) || []
        })
      }
      this.syncNotification()
    } else {
      this.syncNotification()
    }
    //})
  }

  async syncNotification() {
    const notificationData = await getNotificationsHistory()
    const data = await this.notificationsArrayToMap(notificationData)

    if (this._isMounted) {
      AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(data))
        .then(() => {
          this.setState({
            isLoading: false,
            notifications: data
          })
        })
        .catch(e => { console.log('error', e) })
    }
  }

  componentWillUnmount() {
    this._isMounted = false
    //this._listener.remove()
  }

  notificationsArrayToMap = (notifications) => {
    return new Promise(resolve => {
      const data = notifications.reduce((acc, item) => {
        const deliveredAt = moment(new Date(item.delivered_at)).format('MMM YYYY')
        const foundIndex = acc.findIndex(element => element.title === deliveredAt);
        if (foundIndex === -1) {
          return [
            ...acc,
            {
              title: deliveredAt,
              data: [item],
            },
          ];
        }
        acc[foundIndex].data = [...acc[foundIndex].data, item];
        return acc;
      }, []);
      resolve(data)
    })
  }

  htmlDecode = (str) => {
    return str.replace(/&#(\d+);/g, function (match, dec) {
      return String.fromCharCode(dec);
    });
  }

  renderRow = (item) => {
    const deliveredAt = moment(new Date(item.delivered_at)).format('M/DD')
    return (
      <Hyperlink linkDefault={true} linkStyle={styles.hyperLink}>
        <View style={styles.SectionListItem}>
          <Icon style={{ width: '5%', alignSelf: 'center' }} name={'calendar'} />
          <Text style={{ width: '15%', alignSelf: 'center' }}> {deliveredAt} </Text>
          <Text style={{ width: '80%', alignSelf: 'center', justifyContent: 'center' }}> {this.htmlDecode(item.message)} </Text>
        </View>
      </Hyperlink>
    );
  }

  render() {
   
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      )
    }

    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" backgroundColor={colors.primary} />
        {
          !this.state.notifications.length && <CenterMessage message="No more notifications" />
        }

        <SectionList
          renderSectionHeader={({ section }) => <Text style={styles.SectionHeader}> {section.title} </Text>}
          renderItem={({ item }) => this.renderRow(item)}
          keyExtractor={(item, index) => index}
          sections={this.state.notifications}
          ItemSeparatorComponent={() => (
            <View style={{ height: 0.5, width: '100%', backgroundColor: '#b1a99f' }} />
          )}
        />

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: colors.tertiary,
  },
  SectionHeader: {
    backgroundColor: colors.headerBrackground,
    textAlign: 'center',
    fontSize: 20,
    padding: 5,
    color: '#fff',
    fontStyle: 'italic'
  },
  SectionListItem: {
    fontSize: 14,
    padding: 10,
    color: '#000',
    backgroundColor: colors.listBackground,
    flex: 1,
    flexDirection: 'row'
  },
  hyperLink: {
    color: '#2980b9'
  }
})


export default Notifications;
