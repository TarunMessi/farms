import React, { Component } from 'react'
import { View, Text, StyleSheet, StatusBar, ActivityIndicator, TouchableOpacity } from 'react-native';
import { colors } from './styles/theme'
import { Agenda, CalendarList } from 'react-native-calendars';
import { getCalendarEvents } from './component/ApiStore'
import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';
const STORAGE_KEY = 'calendar'
import Icon from "react-native-vector-icons/FontAwesome5";
import stylesTheme from './styles/StyleTheme';
const Entities = require('html-entities').XmlEntities;
const entities = new Entities();

export default class CalendarEvents extends Component {

    _isMounted = false;

    constructor(props) {
        super(props);
        this.state = {
            items: {},
            markedDates: {},
            activityIndicator: true,
            itemsForSelectedDay: {},
        };
    }

    async componentDidMount() {
        StatusBar.setBarStyle('light-content', true);
        if (Platform.OS === 'android') {
            StatusBar.setBackgroundColor(colors.primary);
        }
        this._isMounted = true
        //this._listener = this.props.navigation.addListener('didFocus', async () => {
        var calendarEvents = await AsyncStorage.getItem(STORAGE_KEY);
        if (calendarEvents != null) {
            if (this._isMounted) {
                let data = await this.eventsArrayToMap(JSON.parse(calendarEvents))
            }

            this.syncEvents()
        } else {
            this.syncEvents()
        }
        //})

    }

    async syncEvents() {
        let startFrom = moment().subtract(90, 'days').format('YYYY-MM-DD'); //past 90 days events
        const events = await getCalendarEvents(startFrom)
        if (this._isMounted) {
            let data = await this.eventsArrayToMap(events)
            AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(events)).catch(e => { console.log('error', e) })
        }
    }

    componentWillUnmount() {
        this._isMounted = false
        //this._listener.remove()
    }

    eventsArrayToMap = (events) => {

        return new Promise(async (resolve, reject) => {

            var items = {}, markedDates = {};

            if (events.length > 0) {

                for (let i = 0; i < events.length; i++) {

                    var startDate = moment(events[i].start_date).format('YYYY-MM-DD')
                    var endDate = moment(events[i].end_date).format('YYYY-MM-DD')
                    var time = 'All Day'
                    var multiDay = false

                    if (startDate != endDate) {
                        multiDay = true
                    }
                    if (!events[i].all_day) {
                        time = moment(events[i].start_date).format('h:mm A') + ' - ' + moment(events[i].end_date).format('h:mm A');
                    }

                    if (multiDay) {
                        var dates = this.getDateArr(startDate, endDate)
                        dates.forEach((date) => {

                            if (!items[date]) {
                                items[date] = []
                            }
                            items[date].push(
                                {
                                    time: time,
                                    title: events[i].title,
                                    description: events[i].description,
                                    image: events[i].image ? events[i].image : null,
                                    organizer: (events[i].organizer && events[i].organizer.length > 0) ? events[i].organizer[0].organizer : null,
                                    venue: events[i].venue
                                });
                        })

                    } else {
                        if (!items[startDate]) {
                            items[startDate] = []
                            markedDates[startDate] = { marked: true }
                        }
                        items[startDate].push(
                            {
                                time: time,
                                title: events[i].title,
                                description: events[i].description,
                                image: events[i].image ? events[i].image : null,
                                organizer: (events[i].organizer && events[i].organizer.length > 0) ? events[i].organizer[0].organizer : null,
                                venue: events[i].venue
                            });
                    }
                }
                this.setState({
                    items: items,
                    markedDates: markedDates,
                    activityIndicator: false
                })
            } else {
                this.setState({
                    activityIndicator: false
                })
            }
            resolve(true)

        }).catch((err) => {
            console.log(err)
        })
    }

    // getDateArr = (startDate, endDate) => {
    //     let arr = []
    //     if(startDate != endDate){
    //         arr.push(startDate)
    //         arr.push(endDate)
    //     }else{
    //         let dt = new Date(startDate);
    //         while (dt <= endDate) {
    //             arr.push(new Date(dt));
    //             dt.setDate(dt.getDate() + 1);
    //         }
    //     }
    //     return arr;
    // }
    getDateArr = (startDate, endDate) => {
        let arr = []
        if (startDate == endDate) {
            arr.push(startDate)
            arr.push(endDate)
        } else {
            let dt = new Date(startDate);
            while (dt <= new Date(endDate)) {
                let date = moment(dt).format('YYYY-MM-DD')
                arr.push(date);
                dt.setDate(dt.getDate() + 1);
            }
        }
        return arr;
    }

    renderItem(item) {
        // console.log(item)
        return (
            <TouchableOpacity onPress={() => this.props.navigation.navigate('detailEvent', {
                data: item
            })}>
                <View style={[styles.item]}>
                    <Text>{item.time}</Text>
                    <Text>{entities.decode(item.title)}</Text>
                </View>
            </TouchableOpacity>
        );
    }

    renderEmptyDate() {
        return (
            <View style={styles.emptyDate}></View>
        );
    }

    renderEmptyData() {
        if (!this.state.activityIndicator) {
            return (
                <View style={styles.emptyDate}>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', textAlign: 'center', color: 'grey' }}>No Events on this date</Text>
                </View>
            )
        }
        return (
            <View style={styles.activityContainer}>
                <ActivityIndicator />
            </View>)
    }

    rowHasChanged(r1, r2) {
        return r1.time !== r2.time;
    }

    onUpdateSelectedDate = date => {
        const { items } = this.state;

        const dates = items[date.dateString];

        this.setState({
            itemsForSelectedDay: {
                [date.dateString]: dates
            }
        });
    };

    render() {
        return (
            <View style={styles.container}>
                <StatusBar barStyle="light-content" backgroundColor={colors.primary} />
                <Agenda
                    items={this.state.items}
                    // items={this.state.itemsForSelectedDay}
                    // markedDates={this.state.markedDates}
                    // onDayPress={this.onUpdateSelectedDate.bind(this)}
                    pastScrollRange={4}
                    futureScrollRange={4}
                    selected={moment(new Date()).format('YYYY-MM-DD')}
                    renderItem={this.renderItem.bind(this)}
                    renderEmptyDate={this.renderEmptyDate.bind(this)}
                    rowHasChanged={this.rowHasChanged.bind(this)}
                    renderEmptyData={this.renderEmptyData.bind(this)}
                    renderKnob={() => {return <View>
                        <Icon name="chevron-down" style={{fontSize:20,color:colors.primary}}/>
                        </View>}}
                    theme={{
                        todayBackgroundColor: colors.primary,
                        todayTextColor: '#fff',
                        dotColor: colors.secondary,
                        agendaKnobColor: colors.primary,
                        'stylesheet.agenda.main': {
                            knobContainer: {
                                flex: 1,
                                position: 'absolute',
                                left: 0,
                                right: 0,
                                height: 20,
                                bottom: 0,
                                alignItems: 'center',
                                backgroundColor: '#fff'
                            }
                        }
                    }}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
    },
    item: {
        backgroundColor: 'white',
        flex: 1,
        borderRadius: 5,
        padding: 10,
        marginRight: 10,
        marginTop: 17
    },
    emptyDate: {
        height: 15,
        flex: 1,
        paddingTop: 30
    },
    activityContainer: {
        marginTop: 20,
        alignItems: 'center'
    },
});
