import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  ActivityIndicator,
  SectionList,
  TouchableOpacity
} from 'react-native';
import unescape from 'lodash/unescape';

import CenterMessage from './component/CenterMessage'
import { getDocuments } from './component/ApiStore'
import { colors } from './styles/theme';
import Icon from "react-native-vector-icons/FontAwesome5";
import AsyncStorage from '@react-native-async-storage/async-storage';
const STORAGE_KEY = 'documents';
import stylesTheme from './styles/StyleTheme';

class Documents extends Component {

  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      documents: []
    }
  }

  async componentDidMount() {
    StatusBar.setBarStyle('light-content', true);
    if (Platform.OS === 'android') {
      StatusBar.setBackgroundColor(colors.primary);
    }
    this._isMounted = true;
    var documentsStore = await AsyncStorage.getItem(STORAGE_KEY);
    if (documentsStore != null) {
      if (this._isMounted) {
        this.setState({
          isLoading: false,
          documents: JSON.parse(documentsStore) || []
        })
      }
      this.syncDocuments()
    } else {
      this.syncDocuments()
    }
  }
  async syncDocuments() {
    const documentData = await getDocuments()
    const documents = await this.documentsArrayToMap(documentData)

    if (this._isMounted) {
      AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(documents))
        .then(() => {
          this.setState({
            isLoading: false,
            documents: documents
          })
        })
        .catch(e => { console.log('error', e) })
    }
  }

  documentsArrayToMap = (documents) => {
    return new Promise(resolve => {
      const data = documents.reduce((acc, item) => {
        const category = item.category
        const foundIndex = acc.findIndex(element => element.title === category);
        if (foundIndex === -1) {
          return [
            ...acc,
            {
              title: category,
              data: [item],
            },
          ];
        }
        acc[foundIndex].data = [...acc[foundIndex].data, item];
        return acc;
      }, []);
      resolve(data)
    })
  }

  renderRow = (item) => {
    return (
      <TouchableOpacity onPress={() => (item.url != '') ? this.props.navigation.navigate('PDFViewer', {
        data: {
          title: 'Documents',
          description: item.url,
          downloadTitle: item.title
        }
      }) : false}>
        <View style={styles.SectionListItem}>
          <Text style={{ width: '80%', alignSelf: 'center', justifyContent: 'center' }}> {item.title} </Text>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      )
    }

    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" backgroundColor={colors.primary} />
        {
          !this.state.documents.length && <CenterMessage message="No more documents" />
        }

        <SectionList
          renderSectionHeader={({ section }) => <Text style={styles.SectionHeader}> {unescape(section.title)} </Text>}
          renderItem={({ item }) => this.renderRow(item)}
          keyExtractor={(item, index) => index.toString()}
          sections={this.state.documents}
          ItemSeparatorComponent={() => (
            <View style={{ height: 0.5, width: '100%', backgroundColor: '#b1a99f' }} />
          )}
        />

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: colors.tertiary,
  },
  SectionHeader: {
    backgroundColor: colors.headerBrackground,
    textAlign: 'center',
    fontSize: 20,
    padding: 5,
    color: '#fff',
    fontStyle: 'italic'
  },
  SectionListItem: {
    fontSize: 14,
    padding: 10,
    color: '#000',
    backgroundColor: colors.listBackground,
    flex: 1,
    flexDirection: 'row'
  }
})


export default Documents;