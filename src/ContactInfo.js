import React, { Component } from 'react'
import { Text, View, StatusBar, ActivityIndicator, Linking, StyleSheet, SectionList, TouchableOpacity } from 'react-native';
import { getContactNumbers } from './component/ApiStore'
import { colors } from './styles/theme'
import Icon from "react-native-vector-icons/FontAwesome5";
import CenterMessage from './component/CenterMessage'
import AsyncStorage from '@react-native-async-storage/async-storage';
const STORAGE_KEY = 'contacts';
import stylesTheme from './styles/StyleTheme';
const headerTitleStyles = {
  color: '#fff',
  alignSelf: 'center',
  textAlign: 'center'
}

export default class ContactInfo extends Component {
  _isMounted = false;
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      numbers: []
    }
  }

  async componentDidMount() {
    StatusBar.setBarStyle('light-content', true);
    if (Platform.OS === 'android') {
      StatusBar.setBackgroundColor(colors.primary);
    }
    this._isMounted = true;
    var contactsStored = await AsyncStorage.getItem(STORAGE_KEY);
    if (contactsStored != null) {
      if (this._isMounted) {
        this.setState({
          isLoading: false,
          numbers: JSON.parse(contactsStored) || []
        })
      }
      this.syncContacts()
    } else {
      this.syncContacts()
    }
  }
  async syncContacts() {
    const numbersData = await getContactNumbers()
    const numbers = await this.numbersArrayToMap(numbersData)
    if (this._isMounted) {
      AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(numbers))
        .then(() => {
          this.setState({
            isLoading: false,
            numbers: numbers
          })
        })
        .catch(e => { console.log('error', e) })
    }
  }

  numbersArrayToMap = (numbers) => {
    return new Promise(resolve => {
      const data = numbers.reduce((acc, item) => {
        const contactTitle = item.title
        const foundIndex = acc.findIndex(element => element.title === contactTitle);
        if (foundIndex === -1) {
          return [
            ...acc,
            {
              title: contactTitle,
              data: [item],
            },
          ];
        }
        acc[foundIndex].data = [...acc[foundIndex].data, item];
        return acc;
      }, []);
      resolve(data)
    })
  }
  renderRow = (item) => {
    const name = item.name
    var cleaned = ('' + item.number).replace(/\D/g, '')
    var match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/)
    var number = item.number
    if (match) {
      var intlCode = (match[1] ? '+1 ' : ''),
        number = [intlCode, '(', match[2], ') ', match[3], '-', match[4]].join('');
    }
    return (
      <TouchableOpacity onPress={() => { Linking.openURL(`tel:${item.number}`); }} style={styles.funcNavText}>
        <View style={styles.SectionListItem}>
          <Icon style={{ width: '5%', alignSelf: 'center' }} name={'phone'} />
          <Text style={{ width: '60%', alignSelf: 'center' }}> {name} </Text>
          <Text style={{ width: '35%', alignSelf: 'center', justifyContent: 'center' }}> {number} </Text>
        </View>
      </TouchableOpacity>
    );
  }
  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      )
    }

    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" backgroundColor={colors.primary} />
        {
          !this.state.numbers.length && <CenterMessage message="No more Contacts" />
        }

        <SectionList
          renderSectionHeader={({ section }) => <Text style={styles.SectionHeader}> {section.title} </Text>}
          renderItem={({ item }) => this.renderRow(item)}
          keyExtractor={(item, index) => index}
          sections={this.state.numbers}
          ItemSeparatorComponent={() => (
            <View style={{ height: 0.5, width: '100%', backgroundColor: '#b1a99f' }} />
          )}
        />

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: colors.tertiary,
  },
  SectionHeader: {
    backgroundColor: colors.primary,
    textAlign: 'center',
    fontSize: 20,
    padding: 5,
    color: '#fff',
    fontStyle: 'italic'
  },
  SectionListItem: {
    fontSize: 14,
    padding: 10,
    color: '#000',
    backgroundColor: colors.listBackground,
    flex: 1,
    flexDirection: 'row'
  }
})
