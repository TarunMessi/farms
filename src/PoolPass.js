// import React, { Component } from 'react'
// import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, StatusBar, Dimensions } from 'react-native';
// import { colors } from './styles/theme'
// import AsyncStorage from '@react-native-async-storage/async-storage';
// import Icon from "react-native-vector-icons/FontAwesome5";
// import stylesTheme from './styles/StyleTheme';

// const numColumns = 3
// const STORAGE_KEY = 'poolPass'

// export default class PoolPass extends Component {

//   constructor(props) {
//     super(props);
//     this.state = {
//       poolPass: []
//     };
//   }

//   componentDidMount(){
//     if (Platform.OS === 'android') {
//       StatusBar.setBackgroundColor(colors.primary);
//       StatusBar.setBarStyle('light-content', true);
//     }
//     this.refreshData()
//   }

//   async refreshData () {
//     const poolPass = await AsyncStorage.getItem(STORAGE_KEY)
//     const data = JSON.parse(poolPass) || []
//     this.setState({
//       poolPass : data
//     })
//   }

//   selectItem = (item) => {
//     if(item){
//       this.props.navigation.navigate('showPoolPass',{
//         data:item, 
//         handleDelete: this.deleteItem.bind(this),
//         handleRefresh: this.refreshData.bind(this)
//       })
//     }
//   }

//   async deleteItem (id) {

//     const poolPass = this.state.poolPass.filter(item => {
//         return item.id != id
//     });
//     AsyncStorage.setItem( STORAGE_KEY, JSON.stringify(poolPass))
//     .then(() => { 
//         this.setState({
//           poolPass : JSON.parse(poolPass) || []
//         })
//     } )
//     .catch(e => { console.log('error', e) })
//   }

//   renderItem = ({ item, index }) => {
//     if(item.addPass){
//       return (
//         <View style={styles.item}>
//           <TouchableOpacity
//             onPress={() => this.props.navigation.navigate('addPoolPass',{
//               handleRefresh: this.refreshData.bind(this)
//             })}
//           >
//             <Image style={styles.imageThumbnail} source={require('./assets/poolpass.png')} />
//           </TouchableOpacity>
//         </View>
//       )
//     }else{
//       return (
//         <View style={styles.item}>
//           <TouchableOpacity
//             onPress={() => this.selectItem(item)}
//           >
//               <Image style={styles.imageThumbnail} source={{uri :item.photo.uri}} />
//           </TouchableOpacity>
//         </View>
//       );
//     }
//   };

//   render() {
//     return (
//       <View style={styles.container}>
//       <StatusBar barStyle="light-content" backgroundColor={colors.primary}/>
//         <FlatList
//           data={[...this.state.poolPass, { addPass: true }]}
//           extraData={this.state}
//           numColumns={numColumns}
//           renderItem={this.renderItem}
//           removeClippedSubviews={false}
//           keyExtractor={(item, index) => item.id}
//         />
//       </View>
//     )
//   }
// }

// const styles = StyleSheet.create({
//   container:{
//     flex:1,
//     flexDirection:'column',
//     backgroundColor: colors.tertiary,
//   },
//   imageThumbnail: {
//     height: 200,
//     borderWidth:3,
//     borderRadius:5,
//     borderColor:'#707070',
//     width: ((Dimensions.get('window').width - (10 * numColumns))/ numColumns),
//     resizeMode:'stretch'
//   },
//   item: {
//     margin:2,
//   },
//   icon:{
//     textAlign:"center",
//     fontSize:20
//   }
// })
import React, { Component } from 'react'
import { Text, View } from 'react-native'

export default class PoolPass extends Component {
  render() {
    return (
      <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
      <Text style={{fontSize:20,}}>Coming Soon</Text>
      </View>
    )
  }
}

