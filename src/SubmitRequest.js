import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  StatusBar,
  TouchableOpacity
} from 'react-native';
import { colors } from './styles/theme'
import StepIndicator from 'react-native-step-indicator';

import ProfileInfo from './component/submitRequest/ProfileInfo'
import Description from './component/submitRequest/Description';
import Location from './component/submitRequest/Location';
import Photos from './component/submitRequest/Photos';
import Submit from './component/submitRequest/Submit';
import { submitRequest } from './component/ApiStore';
import Icon from "react-native-vector-icons/FontAwesome5";
import stylesTheme from '../src/styles/StyleTheme';

const labels = ["Profile","Description","Location","Photos","Submit"];
const customStyles = {
  stepIndicatorSize: 25,
  currentStepIndicatorSize:30,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: colors.secondary,
  stepStrokeWidth: 3,
  stepStrokeFinishedColor: colors.secondary,
  stepStrokeUnFinishedColor: '#aaaaaa',
  separatorFinishedColor: colors.secondary,
  separatorUnFinishedColor: '#aaaaaa',
  stepIndicatorFinishedColor: colors.secondary,
  stepIndicatorUnFinishedColor: '#ffffff',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: 13,
  currentStepIndicatorLabelFontSize: 13,
  stepIndicatorLabelCurrentColor: colors.secondary,
  stepIndicatorLabelFinishedColor: '#ffffff',
  stepIndicatorLabelUnFinishedColor: '#aaaaaa',
  labelColor: '#999999',
  labelSize: 11,
  currentStepLabelColor: colors.secondary,
  labelFontFamily: colors.fontFamily
}

class SubmitRequest extends Component {

  constructor (props) {
    super(props)

    this.state = {
      currentPosition: 0,
      totalPage: labels.length - 1,
      name: '',
      email: '',
      phone: '',
      address: '',
      description: '',
      inquiry: '',
      location:{
        latitude: '',
        latitudeDelta: '',
        longitude: '',
        longitudeDelta: ''
      },
      images:[],
      submitSuccess: false
    }
  }
  
  componentDidMount () {
    if (Platform.OS === 'android') {
      StatusBar.setBackgroundColor(colors.primary);
      StatusBar.setBarStyle('light-content', true);
    }
    this.onLoad()
  }
    
  onLoad = () => {
    //this._listener = this.props.navigation.addListener('didFocus', () => {
      this.setState({
          currentPosition: 0,
          totalPage: labels.length - 1,
          name: '',
          email: '',
          phone: '',
          address: '',
          description: '',
          inquiry : '',
          location:{
            latitude: '',
            latitudeDelta: '',
            longitude: '',
            longitudeDelta: ''
          },
          images:[],
          submitSuccess: false
      })
    //})
  }

  // componentWillUnmount () {
  //   this._listener.remove()
  // }

  saveFormData = (data, isSubmit = false) => {
    this.setState({
      ...data
    },async ()=>{
      if(isSubmit){
          this.state.images = this.state.images.map((item)=>{
            return item.rawData
          })
          this.state.phone = this.state.phone.replace(/\D+/g,'')
          let response = {status : 1}//await submitRequest(this.state)
          this.setState({
            submitSuccess: (response.status === 1)
          },() => {
              this.onPageChange(this.state.currentPosition + 1)
          })
          
      }
    })

  }

  onPageChange = (position) => {
    this.setState({currentPosition: position});
  }

  pageComponent = (page) => {
    switch(page) {
 
      case 1:
        return (
          <Description mainState={this.state} saveFormData={this.saveFormData} onPageChange={this.onPageChange}></Description>
        )
        break;
      
      case 2:
        return (
          <Location mainState={this.state} saveFormData={this.saveFormData} onPageChange={this.onPageChange}></Location>
        )
        break;
 
      case 3:
        return (
          <Photos mainState={this.state} saveFormData={this.saveFormData} onPageChange={this.onPageChange}></Photos>
        )
        break;
 
      case 4:
        return (
          <Submit success={this.state.submitSuccess} ></Submit>
        )
        break;
 
      default:
        return (
          <ProfileInfo mainState={this.state} saveFormData={this.saveFormData} onPageChange={this.onPageChange}></ProfileInfo>
        )
      }
  }

  render () {
    return (
          <View style={styles.container}>
          <StatusBar barStyle="light-content" backgroundColor={colors.primary}/>
              <View style={styles.stepIndicator}>
                <StepIndicator
                  customStyles={customStyles}
                  currentPosition={this.state.currentPosition}
                  labels={labels}
                />
              </View>
                {
                  this.pageComponent(this.state.currentPosition)
                }
          </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.tertiary,
  },
  stepIndicator: {
    marginVertical:20,
    paddingHorizontal:2
  },

  
});

export default SubmitRequest;