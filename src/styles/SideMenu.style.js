import { colors } from './theme'

export default {
    container: {
      flex: 1
    },
    brandStyle: {
      padding: 10,
      borderBottomColor : '#fff',
      borderBottomWidth : 1
    },
    brandText: {
      fontSize: 20,
      color: '#fff',
      fontWeight: 'bold'
    },
    navIcons:{
      color : colors.tertiary,
      fontSize: 16
    },
    navItemStyle: {
      padding: 15,
      fontSize: 16,
      color: '#fff'
    },
    navSectionStyle: {
      borderBottomColor : '#fff',
      borderBottomWidth : 1
    }
  };