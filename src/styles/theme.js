const colors = {
    /** Vista Filare */
    primary : '#436db1',
    secondary : '#dea849',
    tertiary : '#fff',
    separatorLine : '#fff',
    finalColor: '#000',
    // Section Header 
    headerBrackground : '#19432d',
    listBackground: 'rgb(241, 241, 241)',

}

export {
    colors
}