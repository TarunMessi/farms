import { colors } from './theme';
export default {
    container: {
        flex: 1
    },
    buttonStyle: {
        justifyContent: 'center',
        margin: 50,
        backgroundColor: colors.iconColor
    }, buttonText: {
        color: '#000',
        fontFamily:colors.fontFamily,
        fontSize: 18
    },headerLeftIcon: {
        color: '#fff',
        fontSize: 20
    }

};